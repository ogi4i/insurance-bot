package http

import (
	"errors"
	"log"
	"net/http"

	"github.com/go-chi/render"

	"insurance_bot/internal/app/pkg/domain"
)

type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	log.Printf("status_code: %d; status_text: %s; error: %s", e.HTTPStatusCode, e.StatusText, e.ErrorText)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "invalid request",
		ErrorText:      err.Error(),
	}
}

func ErrInternalServerError(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 500,
		StatusText:     "internal server error",
		ErrorText:      err.Error(),
	}
}

var ErrNotFound = &ErrResponse{HTTPStatusCode: 404, StatusText: "resource not found"}

func ErrorToHTTP(err error) render.Renderer {
	var domainErr domain.Error
	if ok := errors.As(err, &domainErr); ok {
		if domainErr == domain.ErrInternalServerError {
			return &ErrResponse{
				Err:            err,
				HTTPStatusCode: 500,
				StatusText:     "internal server error",
				ErrorText:      err.Error(),
			}
		}
	}

	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 500,
		StatusText:     "internal server error",
		ErrorText:      err.Error(),
	}
}
