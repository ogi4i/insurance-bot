package notification_form_handler

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"

	"insurance_bot/internal/app/pkg/domain"
	mux "insurance_bot/internal/app/pkg/http"
)

func (h *Handler) EnsureUUIDExists(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if uuid := chi.URLParam(r, "uuid"); uuid != "" {
			if h.service.DoesUUIDExist(uuid) {
				next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), domain.UUIDContextKey, uuid)))
			}
		} else {
			_ = render.Render(w, r, mux.ErrNotFound)
			return
		}
	})
}

func (h *Handler) BasicAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, _ := r.BasicAuth()

		if !h.checkNotificationFormAuth(username, password) {
			w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func (h *Handler) checkNotificationFormAuth(username, password string) bool {
	for _, auth := range h.authKeys {
		if auth.Username == username && auth.Password == password {
			return true
		}
	}
	return false
}
