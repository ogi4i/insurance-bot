package notification_form_handler

import (
	"net/http"

	"github.com/go-chi/render"

	"insurance_bot/internal/app/insurance_bot"
	"insurance_bot/internal/app/pkg/domain"
	mux "insurance_bot/internal/app/pkg/http"
)

type (
	Handler struct {
		service  insurance_bot.Service
		authKeys []*domain.Authentication
	}
)

const (
	errorURI = "/error"
)

func NewHandler(service insurance_bot.Service, authKeys []*domain.Authentication) *Handler {
	return &Handler{
		service:  service,
		authKeys: authKeys,
	}
}

func (h *Handler) GetForm(w http.ResponseWriter, r *http.Request) {
	if err := h.service.GetForm(w); err != nil {
		_ = render.Render(w, r, mux.ErrInternalServerError(err))
		return
	}
}

func (h *Handler) NewNotification(w http.ResponseWriter, r *http.Request) {
	if !isValidNewNotificationRequest(r) {
		http.Redirect(w, r, errorURI, http.StatusSeeOther)
		return
	}

	if err := h.service.NewNotification(
		r.Context(),
		w,
		r.PostFormValue(string(domain.TextAreaPostFormField)),
		r.PostForm[string(domain.InsuranceTypePostFormField)],
		r.PostFormValue(string(domain.FormatPostFormField)),
	); err != nil {
		_ = render.Render(w, r, mux.ErrInternalServerError(err))
		return
	}
}

func (h *Handler) ConfirmNotification(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value(domain.UUIDContextKey).(string)
	if err := h.service.ConfirmNotification(w, id); err != nil {
		_ = render.Render(w, r, mux.ErrInternalServerError(err))
		return
	}
	render.Status(r, http.StatusCreated)
}

func (h *Handler) GetErrorForm(w http.ResponseWriter, r *http.Request) {
	if err := h.service.GetErrorForm(w); err != nil {
		_ = render.Render(w, r, mux.ErrInternalServerError(err))
		return
	}
}

func isValidNewNotificationRequest(r *http.Request) bool {
	if err := r.ParseForm(); err != nil {
		return false
	}

	if r.PostFormValue("textarea") == "" ||
		r.PostFormValue("format") == "" ||
		r.PostFormValue(string(domain.InsuranceTypeLeadCustomFieldType)) == "" {
		return false
	}

	return true
}
