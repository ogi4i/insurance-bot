package bot_handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/go-chi/render"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/insurance_bot"
	mux "insurance_bot/internal/app/pkg/http"
)

type Handler struct {
	service insurance_bot.Service
}

func NewHandler(service insurance_bot.Service) *Handler {
	return &Handler{service: service}
}

func (h *Handler) HandleUpdate(w http.ResponseWriter, r *http.Request) {
	bytes, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		_ = render.Render(w, r, mux.ErrInternalServerError(err))
		return
	}

	update := new(tgbotapi.Update)
	err = json.Unmarshal(bytes, update)
	if err != nil {
		_ = render.Render(w, r, mux.ErrInvalidRequest(err))
		return
	}

	err = h.service.HandleUpdate(r.Context(), update)
	if err != nil {
		_ = render.Render(w, r, mux.ErrorToHTTP(err))
		return
	}

	w.WriteHeader(http.StatusOK)
}
