package bot_handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func LogTelegramWebhooks(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := new(tgbotapi.Update)

		bodyBytes, _ := ioutil.ReadAll(r.Body)
		r.Body.Close()
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.Unmarshal(bodyBytes, data); err == nil {
			logText := new(strings.Builder)
			if data.Message != nil {
				logText.WriteString(fmt.Sprintf("Request text - Created: %s; From: %d", data.Message.Time().Format(time.RFC3339), data.Message.From.ID))
				if data.Message.IsCommand() {
					logText.WriteString(fmt.Sprintf("; Command: %s; Arguments: %s", data.Message.Command(), data.Message.CommandArguments()))
				}

				if data.Message.Contact != nil {
					logText.WriteString(fmt.Sprintf("; Phone number: %s", data.Message.Contact.PhoneNumber))
				} else {
					logText.WriteString(fmt.Sprintf("; Text: %s", data.Message.Text))
				}
			} else if data.CallbackQuery != nil {
				logText.WriteString(fmt.Sprintf("Request callback - Created: %s; From: %d; CallbackData: %s", data.CallbackQuery.Message.Time().Format(time.RFC3339), data.CallbackQuery.From.ID, data.CallbackQuery.Data))
			}

			log.Print(logText.String())
		}

		next.ServeHTTP(w, r)
	})
}
