package amocrm

import (
	"context"
	"time"

	"github.com/ogi4i/amocrm-client"

	"insurance_bot/internal/app/pkg/domain"
)

func (c *Client) SearchContact(ctx context.Context, params *amocrm.ContactRequestParams) (*domain.Contact, error) {
	contacts := make([]*amocrm.Contact, 0)
	err := c.retry(ctx, c.retryCount, func() (err error) {
		contacts, err = c.client.GetContacts(ctx, params)
		return err
	})
	if err != nil {
		return nil, err
	}

	if len(contacts) == 1 {
		return c.convertToContact(contacts[0]), nil
	}

	return nil, nil
}

func (c *Client) AddPromocode(ctx context.Context, contact *domain.Contact, promocode string) error {
	err := c.retry(ctx, c.retryCount, func() (err error) {
		_, err = c.client.UpdateContact(ctx, &amocrm.ContactUpdate{
			ID:        contact.ID,
			UpdatedAt: int(time.Now().Unix()),
			CustomFields: []*amocrm.UpdateCustomField{{
				ID: c.contactsCustomFields[domain.PromocodeContactProperty].ID,
				Values: []interface{}{amocrm.UpdateCustomValue{
					Value: promocode,
				}},
			}},
		})
		return err
	})

	if err != nil {
		return err
	}

	return nil
}
