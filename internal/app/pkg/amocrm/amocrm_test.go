package amocrm

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetKeyFromValue(t *testing.T) {

	m := map[string]string{
		"1": "2",
		"3": "4",
		"5": "6",
	}

	t.Run("get_with_existant_value", func(t *testing.T) {
		assert.Equal(t, "5", getKeyFromValue(m, "6"))
	})

	t.Run("get_with_non_existant_value", func(t *testing.T) {
		assert.Equal(t, "", getKeyFromValue(m, "7"))
	})

	t.Run("get_with_empty_value", func(t *testing.T) {
		assert.Equal(t, "", getKeyFromValue(m, ""))
	})
}

func TestNormalizePhoneNumber(t *testing.T) {
	type testCase struct {
		input    string
		expected string
	}

	tt := []testCase{
		{
			input:    "9161234567",
			expected: "+79161234567",
		},
		{
			input:    "+7916-123-45-67",
			expected: "+79161234567",
		},
		{
			input:    "8(916)123-45-67",
			expected: "+79161234567",
		},
		{
			input:    "916    123---45-67",
			expected: "+79161234567",
		},
	}

	for _, test := range tt {
		assert.Equal(t, test.expected, normalizePhoneNumber(test.input))
	}
}
