package amocrm

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/ogi4i/amocrm-client"

	"insurance_bot/internal/app/config"
	"insurance_bot/internal/app/pkg/domain"
)

type (
	AmoCRM interface {
		Authorize(ctx context.Context) error
		GetAccount(ctx context.Context, reqParams *amocrm.AccountRequestParams) (*amocrm.AccountResponse, error)
		AddContact(ctx context.Context, contact *amocrm.ContactAdd) (int, error)
		UpdateContact(ctx context.Context, contact *amocrm.ContactUpdate) (int, error)
		GetContacts(ctx context.Context, reqParams *amocrm.ContactRequestParams) ([]*amocrm.Contact, error)
		AddLead(ctx context.Context, lead *amocrm.LeadAdd) (int, error)
		UpdateLead(ctx context.Context, lead *amocrm.LeadUpdate) (int, error)
		GetLeads(ctx context.Context, reqParams *amocrm.LeadRequestParams) ([]*amocrm.Lead, error)
		AddNote(ctx context.Context, note *amocrm.NoteAdd) (int, error)
		GetNotes(ctx context.Context, reqParams *amocrm.NoteRequestParams) ([]*amocrm.Note, error)
		DownloadAttachment(ctx context.Context, attachment string) ([]byte, error)
		GetPipelines(ctx context.Context, reqParams *amocrm.PipelineRequestParams) (map[string]*amocrm.Pipeline, error)
		AddTask(ctx context.Context, task *amocrm.TaskAdd) (int, error)
		UpdateTask(ctx context.Context, task *amocrm.TaskUpdate) (int, error)
		GetTasks(ctx context.Context, reqParams *amocrm.TaskRequestParams) ([]*amocrm.Task, error)
	}

	Client struct {
		client                   AmoCRM
		retryCount               int
		pipelineID               int
		pipelineStatusesByID     map[int]domain.LeadStatus
		pipelineStatuses         map[domain.LeadStatus]*amocrm.PipelineStatus
		taskTypesByID            map[int]string
		taskTypes                map[string]*amocrm.TaskType
		contactsCustomFieldsByID map[int]domain.ContactProperty
		contactsCustomFields     map[domain.ContactProperty]*amocrm.CustomFieldInfo
		leadsCustomFieldsByID    map[int]*config.LeadsCustomFields
		leadsCustomFields        map[domain.LeadCustomFieldType]*amocrm.CustomFieldInfo
	}
)

func NewAmoCRMClient(ctx context.Context, config *config.AmoCRM) (*Client, error) {
	client, err := amocrm.NewClient(config.Domain, config.Login, config.APIKey, amocrm.WithHTTPTimeout(3*time.Second))
	if err != nil {
		return nil, err
	}

	err = client.Authorize(ctx)
	if err != nil {
		return nil, err
	}

	accountResponse, err := client.GetAccount(ctx, &amocrm.AccountRequestParams{
		With: []amocrm.AccountWithType{
			amocrm.AccountWithCustomFields,
			amocrm.AccountWithPipelines,
			amocrm.AccountWithTaskTypes,
		},
	})
	if err != nil {
		return nil, err
	}

	pipelineStatuses := make(map[domain.LeadStatus]*amocrm.PipelineStatus)
	for _, status := range accountResponse.Embedded.Pipelines[strconv.Itoa(config.Pipeline.ID)].Statuses {
		if name, ok := config.Pipeline.Statuses[status.ID]; ok {
			pipelineStatuses[name] = status
		}
	}

	taskTypes := make(map[string]*amocrm.TaskType)
	for _, taskType := range accountResponse.Embedded.TaskTypes {
		if name, ok := config.TaskTypes[taskType.ID]; ok {
			taskTypes[name] = taskType
		}
	}

	contactsCustomFields := make(map[domain.ContactProperty]*amocrm.CustomFieldInfo)
	for _, field := range accountResponse.Embedded.CustomFields.Contacts {
		if name, ok := config.CustomFields.Contacts[field.ID]; ok {
			contactsCustomFields[name] = field
		}
	}

	leadsCustomFields := make(map[domain.LeadCustomFieldType]*amocrm.CustomFieldInfo)
	for _, field := range accountResponse.Embedded.CustomFields.Leads {
		if customField, ok := config.CustomFields.Leads[field.ID]; ok {
			leadsCustomFields[customField.Type] = field
		}
	}

	return &Client{
		client:                   client,
		retryCount:               config.RetryCount,
		pipelineID:               config.Pipeline.ID,
		pipelineStatusesByID:     config.Pipeline.Statuses,
		pipelineStatuses:         pipelineStatuses,
		taskTypesByID:            config.TaskTypes,
		taskTypes:                taskTypes,
		contactsCustomFieldsByID: config.CustomFields.Contacts,
		contactsCustomFields:     contactsCustomFields,
		leadsCustomFieldsByID:    config.CustomFields.Leads,
		leadsCustomFields:        leadsCustomFields,
	}, nil
}

func (c *Client) handleAmoCRMError(ctx context.Context, handledErr error) error {
	if handledErr != nil {
		if amoErr, ok := handledErr.(*amocrm.AmoError); ok {
			if amoErr.ErrorCode == amocrm.InvalidCredentialsCode {
				return c.client.Authorize(ctx)
			}
		}
	}
	return handledErr
}

func (c *Client) retry(ctx context.Context, attempts int, f func() error) (err error) {
	for i := 0; i < attempts; i++ {
		err = f()
		if err == nil {
			return
		}

		err = c.handleAmoCRMError(ctx, err)
		if err != nil {
			return
		}
	}

	return fmt.Errorf("failed retrying after %d attempts, last error: %v", attempts, err)
}

func (c *Client) fillCustomFieldsPost(data interface{}) []*amocrm.UpdateCustomField {
	result := make([]*amocrm.UpdateCustomField, 0)
	switch v := data.(type) {
	case []*domain.UserActionProperty:
		for _, property := range v {
			if property.IsSelected == 1 {
				if customField, ok := c.leadsCustomFields[domain.LeadCustomFieldType(property.Type)]; ok {
					result = fillCustomFieldPost(property.Name, customField, result)
				}
			}
		}
	case map[string][]string:
		for key, values := range v {
			if customField, ok := c.leadsCustomFields[domain.LeadCustomFieldType(key)]; ok &&
				c.leadsCustomFieldsByID[c.leadsCustomFields[domain.LeadCustomFieldType(key)].ID].IsIncludedInNewLead {
				for _, value := range values {
					result = fillCustomFieldPost(value, customField, result)
				}
			}
		}
	}

	return result
}

func hasCustomFieldID(id int, customFields []*amocrm.UpdateCustomField) (*amocrm.UpdateCustomField, bool) {
	for _, f := range customFields {
		if f.ID == id {
			return f, true
		}
	}

	return nil, false
}

func fillCustomFieldPost(value string, customField *amocrm.CustomFieldInfo, result []*amocrm.UpdateCustomField) []*amocrm.UpdateCustomField {
	if v, ok := hasCustomFieldID(customField.ID, result); ok {
		switch customField.FieldType {
		case amocrm.MultiSelectCustomFieldType:
			v.Values = append(v.Values, getKeyFromValue(customField.Enums, value))
		case amocrm.SelectCustomFieldType:
			v.Values = append(v.Values, amocrm.UpdateCustomValue{
				Value: getKeyFromValue(customField.Enums, value),
			})
		default:
			v.Values = append(v.Values, amocrm.UpdateCustomValue{
				Value: value,
			})
		}
	} else {
		switch customField.FieldType {
		case amocrm.MultiSelectCustomFieldType:
			result = append(result, &amocrm.UpdateCustomField{
				ID:     customField.ID,
				Values: []interface{}{getKeyFromValue(customField.Enums, value)},
			})
		case amocrm.SelectCustomFieldType:
			result = append(result, &amocrm.UpdateCustomField{
				ID: customField.ID,
				Values: []interface{}{amocrm.UpdateCustomValue{
					Value: getKeyFromValue(customField.Enums, value),
				}},
			})
		default:
			result = append(result, &amocrm.UpdateCustomField{
				ID: customField.ID,
				Values: []interface{}{amocrm.UpdateCustomValue{
					Value: value,
				}},
			})
		}
	}

	return result
}

func getKeyFromValue(m map[string]string, value string) string {
	for k, v := range m {
		if v == value {
			return k
		}
	}

	return ""
}
