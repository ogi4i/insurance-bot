package amocrm

import (
	"context"
	"path"
	"strconv"
	"time"

	"golang.org/x/sync/errgroup"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/ogi4i/amocrm-client"

	"insurance_bot/internal/app/pkg/domain"
)

const (
	pdfExtension = ".pdf"
)

func (c *Client) GetLeadsByContactIDs(ctx context.Context, contactIds []int) (map[int][]*domain.Lead, error) {
	contacts := make([]*amocrm.Contact, 0)
	err := c.retry(ctx, c.retryCount, func() (err error) {
		contacts, err = c.client.GetContacts(ctx, &amocrm.ContactRequestParams{ID: contactIds})
		return err
	})
	if err != nil {
		return nil, err
	}

	if len(contacts) == 0 {
		return nil, nil
	}

	leadIds := make([]int, 0, len(contacts))
	for _, contact := range contacts {
		leadIds = append(leadIds, contact.Leads.ID...)
	}

	leads := make([]*amocrm.Lead, 0, len(leadIds))
	err = c.retry(ctx, c.retryCount, func() (err error) {
		leads, err = c.client.GetLeads(ctx, &amocrm.LeadRequestParams{ID: leadIds})
		return err
	})
	if err != nil {
		return nil, err
	}

	results := make(map[int][]*domain.Lead, len(leads))
	for _, l := range leads {
		results[l.MainContact.ID] = append(results[l.MainContact.ID], c.convertToLead(l))
	}

	return results, nil
}

func (c *Client) GetLeadByLeadID(ctx context.Context, leadID int) (*domain.Lead, error) {
	leads := make([]*amocrm.Lead, 0)
	err := c.retry(ctx, c.retryCount, func() (err error) {
		leads, err = c.client.GetLeads(ctx, &amocrm.LeadRequestParams{ID: []int{leadID}})
		return err
	})
	if err != nil {
		return nil, err
	}

	if len(leads) == 0 {
		return nil, nil
	}

	return c.convertToLead(leads[0]), nil
}

func (c *Client) CreateNewLead(ctx context.Context, data interface{}) error {
	var err error
	switch v := data.(type) {
	case *domain.UserAction:
		err = c.retry(ctx, c.retryCount, func() (err error) {
			_, err = c.client.AddLead(ctx, &amocrm.LeadAdd{
				Name:         v.Contact.Name,
				StatusID:     c.pipelineStatuses["requested"].ID,
				ContactsID:   []string{strconv.Itoa(v.Contact.ID)},
				CustomFields: c.fillCustomFieldsPost(v.Properties),
			})
			return err
		})
	case *domain.Lead:
		err = c.retry(ctx, c.retryCount, func() (err error) {
			_, err = c.client.AddLead(ctx, &amocrm.LeadAdd{
				Name:         v.Name,
				StatusID:     c.pipelineStatuses["requested"].ID,
				ContactsID:   []string{strconv.Itoa(v.MainContactID)},
				CustomFields: c.fillCustomFieldsPost(v.CustomFields),
			})
			return err
		})
	}

	if err != nil {
		return err
	}

	return nil
}

func (c *Client) AddExtensionFlagToLead(ctx context.Context, lead *domain.Lead) error {
	err := c.retry(ctx, c.retryCount, func() (err error) {
		_, err = c.client.UpdateLead(ctx, &amocrm.LeadUpdate{
			ID:        lead.ID,
			UpdatedAt: int(time.Now().Unix()),
			CustomFields: []*amocrm.UpdateCustomField{{
				ID: c.leadsCustomFields["is_extended"].ID,
				Values: []interface{}{amocrm.UpdateCustomValue{
					Value: domain.TrueString,
				}},
			}},
		})
		return err
	})

	if err != nil {
		return err
	}

	return nil
}

func (c *Client) GetPDFAttachmentsFromLead(ctx context.Context, leadID int) ([]*tgbotapi.FileBytes, error) {
	params := &amocrm.NoteRequestParams{
		Type:      amocrm.LeadNoteType,
		ElementID: []int{leadID},
	}
	notes := make([]*amocrm.Note, 0)
	err := c.retry(ctx, c.retryCount, func() (err error) {
		notes, err = c.client.GetNotes(ctx, params)
		return err
	})
	if err != nil {
		return nil, err
	}

	attachments := make(chan *tgbotapi.FileBytes, len(notes))
	g, ctx := errgroup.WithContext(ctx)
	for _, note := range notes {
		if note.Attachment != "" {
			if path.Ext(note.Parameters.Text) == pdfExtension {
				note := note
				g.Go(func() error {
					var data []byte
					data, err = c.client.DownloadAttachment(ctx, note.Attachment)
					if err != nil {
						return err
					}

					attachments <- &tgbotapi.FileBytes{
						Name:  note.Parameters.Text,
						Bytes: data,
					}
					return nil
				})
			}
		}
	}

	err = g.Wait()
	if err != nil {
		return nil, err
	}
	close(attachments)

	results := make([]*tgbotapi.FileBytes, 0, len(attachments))
	for a := range attachments {
		results = append(results, a)
	}

	return results, nil
}

func (c *Client) ChangeLeadStatusTo(ctx context.Context, leadID int, status domain.LeadStatus) error {
	err := c.retry(ctx, c.retryCount, func() (err error) {
		_, err = c.client.UpdateLead(ctx, &amocrm.LeadUpdate{
			ID:        leadID,
			UpdatedAt: int(time.Now().Unix()),
			StatusID:  c.pipelineStatuses[status].ID,
		})
		return err
	})

	if err != nil {
		return err
	}

	return nil
}

func (c *Client) LeadHasPDFAttachments(ctx context.Context, leadID int) bool {
	params := &amocrm.NoteRequestParams{
		Type:      amocrm.LeadNoteType,
		ElementID: []int{leadID},
	}
	notes := make([]*amocrm.Note, 0)
	err := c.retry(ctx, c.retryCount, func() (err error) {
		notes, err = c.client.GetNotes(ctx, params)
		return err
	})
	if err != nil {
		return false
	}

	for _, note := range notes {
		if note.Attachment != "" {
			if path.Ext(note.Parameters.Text) == pdfExtension {
				return true
			}
		}
	}

	return false
}

func (c *Client) IsLeadInOneOfStatuses(lead *domain.Lead, statuses ...domain.LeadStatus) bool {
	for _, status := range statuses {
		if lead.StatusID == c.pipelineStatuses[status].ID {
			return true
		}
	}
	return false
}
