package amocrm

import (
	"context"
	"time"

	"golang.org/x/sync/errgroup"

	"github.com/ogi4i/amocrm-client"
)

func (c *Client) CompleteExtensionTask(ctx context.Context, leadID int) error {
	tasks := make([]*amocrm.Task, 0)
	err := c.retry(ctx, c.retryCount, func() (err error) {
		tasks, err = c.client.GetTasks(ctx, &amocrm.TaskRequestParams{
			ElementID: []int{leadID},
			Type:      amocrm.LeadTaskType,
			Filter: &amocrm.TaskRequestFilter{
				Status:   amocrm.InProgressStatusTaskFilter,
				TaskType: []int{c.taskTypes["extension"].ID},
			},
		})
		return err
	})
	if err != nil {
		return err
	}

	if len(tasks) == 0 {
		return nil
	}

	g, ctx := errgroup.WithContext(ctx)
	for _, task := range tasks {
		if !task.IsCompleted {
			task := task
			g.Go(func() error {
				taskID, err := c.client.UpdateTask(ctx, &amocrm.TaskUpdate{
					ID:          task.ID,
					Text:        task.Text,
					UpdatedAt:   int(time.Now().Unix()),
					IsCompleted: true,
				})

				if err != nil || taskID == 0 {
					return err
				}

				return nil
			})
		}
	}

	return g.Wait()
}
