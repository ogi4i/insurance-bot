package amocrm

import (
	"regexp"
	"time"

	"github.com/ogi4i/amocrm-client"

	"insurance_bot/internal/app/pkg/domain"
)

func (c *Client) GetLeadCustomFields(customFields ...domain.LeadCustomFieldType) map[domain.LeadCustomFieldType][]string {
	result := make(map[domain.LeadCustomFieldType][]string)
	for _, s := range customFields {
		if f, ok := c.leadsCustomFields[s]; ok {
			list := make([]string, len(f.Enums))
			i := 0
			for _, v := range f.Enums {
				list[i] = v
				i++
			}

			result[s] = list
		}
	}

	return result
}

func (c *Client) convertToLead(lead *amocrm.Lead) *domain.Lead {
	props := make(map[domain.LeadCustomFieldType][]string)

	for _, customField := range lead.CustomFields {
		values := make([]string, len(customField.Values))
		for i, value := range customField.Values {
			values[i] = value.Value
		}

		leadCustomField, ok := c.leadsCustomFieldsByID[customField.ID]
		if !ok {
			continue
		}

		props[leadCustomField.Type] = values
	}

	return &domain.Lead{
		ID:                lead.ID,
		Name:              lead.Name,
		ResponsibleUserID: lead.ResponsibleUserID,
		CreatedBy:         lead.CreatedBy,
		CreatedAt:         time.Unix(int64(lead.CreatedAt), 0),
		UpdatedAt:         time.Unix(int64(lead.UpdatedAt), 0),
		AccountID:         lead.AccountID,
		IsDeleted:         lead.IsDeleted,
		MainContactID:     lead.MainContact.ID,
		GroupID:           lead.GroupID,
		ClosedAt:          time.Unix(int64(lead.ClosedAt), 0),
		ClosestTaskAt:     time.Unix(int64(lead.ClosestTaskAt), 0),
		Tags:              lead.Tags,
		CustomFields:      props,
		Contacts:          lead.Contact.ID,
		StatusID:          lead.StatusID,
		Sale:              lead.Sale,
		PipelineID:        lead.Pipeline.ID,
	}
}

func (c *Client) convertToContact(contact *amocrm.Contact) *domain.Contact {
	props := make(map[domain.ContactProperty][]string)

	for _, customField := range contact.CustomFields {
		values := make([]string, len(customField.Values))
		for i, value := range customField.Values {
			if c.contactsCustomFieldsByID[customField.ID] == domain.PhoneNumberContactProperty {
				values[i] = normalizePhoneNumber(value.Value)
			} else {
				values[i] = value.Value
			}
		}

		contactsCustomField, ok := c.contactsCustomFieldsByID[customField.ID]
		if !ok {
			continue
		}

		props[contactsCustomField] = values
	}

	return &domain.Contact{
		ID:          contact.ID,
		Name:        contact.Name,
		PhoneNumber: props[domain.PhoneNumberContactProperty][0],
		Properties:  props,
	}
}

func normalizePhoneNumber(input string) string {
	//replace first 7 and 8 for +7
	output := regexp.MustCompile("(^8|^7)").ReplaceAllString(input, "+7")
	//replace all non-digit chars
	output = regexp.MustCompile("[^0-9+]+").ReplaceAllString(output, "")
	//add first +7 if missing
	if output[0:2] != "+7" {
		output = "+7" + output
	}

	return output
}
