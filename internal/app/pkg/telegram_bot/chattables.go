package telegram_bot

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"golang.org/x/sync/errgroup"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

const (
	markdownParseMode = "markdown"
)

func (b *Bot) SendMessageWithRetry(ctx context.Context, msg tgbotapi.Chattable) error {
	var message tgbotapi.Message
	var err error
	for i := 0; i < b.config.RetryCount; i++ {
		message, err = b.api.Send(msg)
		if err != nil {
			ticker := time.NewTicker(b.config.RetryBackoff)
			select {
			case <-ticker.C:
				continue
			case <-ctx.Done():
				return ctx.Err()
			}
		}
		break
	}

	if err != nil {
		return fmt.Errorf("failed trying to send message after %d attempts, last error: %q", b.config.RetryCount, err)
	}

	switch msg.(type) {
	case *tgbotapi.MessageConfig:
		log.Printf("Response - To: %d; Text: %s", message.Chat.ID, strings.ReplaceAll(message.Text, "\n", `\n`))
	case *tgbotapi.EditMessageTextConfig:
		log.Printf("Response - To: %d; Text: %s", message.Chat.ID, strings.ReplaceAll(message.Text, "\n", `\n`))
	case *tgbotapi.DocumentConfig:
		log.Printf("Document - To: %d; Filename: %s; Type: %s; Size: %d", message.Chat.ID, message.Document.FileName, message.Document.MimeType, message.Document.FileSize)
	}

	return nil
}

func (b *Bot) SendDocuments(ctx context.Context, userID int64, filepaths []string) error {
	g, _ := errgroup.WithContext(ctx)
	for _, f := range filepaths {
		filepath := f
		g.Go(func() error {
			return b.SendMessageWithRetry(ctx, NewDocument(userID, filepath))
		})
	}

	return g.Wait()
}

func NewMessage(chatID int64, text string, keyboard interface{}, markdown bool) *tgbotapi.MessageConfig {
	msg := tgbotapi.NewMessage(chatID, text)

	if markdown {
		msg.ParseMode = markdownParseMode
	}

	if keyboard != nil {
		msg.ReplyMarkup = keyboard
	}

	return &msg
}

func EditMessage(chatID int64, messageID int, text string, keyboard *tgbotapi.InlineKeyboardMarkup, markdown bool) *tgbotapi.EditMessageTextConfig {
	msg := tgbotapi.NewEditMessageText(chatID, messageID, text)

	if markdown {
		msg.ParseMode = markdownParseMode
	}

	if keyboard != nil {
		msg.ReplyMarkup = keyboard
	}

	return &msg
}

func NewDocument(chatID int64, file interface{}) *tgbotapi.DocumentConfig {
	msg := tgbotapi.NewDocumentUpload(chatID, file)

	return &msg
}
