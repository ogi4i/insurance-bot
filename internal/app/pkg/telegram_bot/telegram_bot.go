package telegram_bot

import (
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/config"
)

type Bot struct {
	api    *tgbotapi.BotAPI
	config *config.Telegram
}

func NewTelegramBot(telegramConfig *config.Telegram) (*Bot, error) {
	bot, err := tgbotapi.NewBotAPI(telegramConfig.Token)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize bot with error: %q", err)
	}

	log.Printf("authorized in account @%s", bot.Self.UserName)

	hook, err := bot.GetWebhookInfo()
	if err != nil {
		return nil, fmt.Errorf("failed to get webhook info from server with error: %q", err)
	}

	if hook.URL != telegramConfig.WebHookDomain+bot.Token {
		_, err = bot.SetWebhook(tgbotapi.NewWebhook(telegramConfig.WebHookDomain + bot.Token))
		if err != nil {
			return nil, fmt.Errorf("failed to set webhook with error: %q", err)
		}
	}

	bot.Client.Timeout = telegramConfig.Timeout

	return &Bot{
		api:    bot,
		config: telegramConfig,
	}, nil
}
