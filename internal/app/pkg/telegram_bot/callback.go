package telegram_bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/pkg/domain"
)

func (b *Bot) AnswerCallback(callback tgbotapi.CallbackConfig) error {
	resp, err := b.api.AnswerCallbackQuery(callback)
	if err != nil {
		return err
	}

	if resp.ErrorCode > 499 {
		return domain.ErrInternalServerError
	}

	return nil
}
