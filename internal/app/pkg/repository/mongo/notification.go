package mongo

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"insurance_bot/internal/app/pkg/domain"
)

func (s *Storage) InsertNotifications(ctx context.Context, until int, contactID int, leadIDs []int) error {
	res := s.collections["users"].FindOneAndUpdate(
		ctx,
		bson.M{"contact.id": contactID},
		bson.M{"$push": bson.M{fmt.Sprintf("notifications_sent_on_leads.%d", until): bson.M{"$each": leadIDs}}},
		options.FindOneAndUpdate().SetReturnDocument(options.After),
	)
	if res.Err() != nil {
		return res.Err()
	}

	data := new(domain.User)
	err := res.Decode(data)
	if err != nil {
		return err
	}

	return nil
}
