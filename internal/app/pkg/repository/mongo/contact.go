package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"insurance_bot/internal/app/pkg/domain"
)

func (s *Storage) BindUserIDToContact(ctx context.Context, userID int, contact *domain.Contact) (*domain.User, error) {
	res := s.collections["users"].FindOneAndUpdate(
		ctx,
		bson.M{"tg_user_id": userID},
		bson.M{"$set": bson.M{"contact": contact}},
		options.FindOneAndUpdate().SetReturnDocument(options.After),
		options.FindOneAndUpdate().SetUpsert(true),
	)
	if res.Err() != nil {
		return nil, res.Err()
	}

	data := new(domain.User)
	err := res.Decode(data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (s *Storage) GetContact(ctx context.Context, userID int) (*domain.Contact, error) {
	res := s.collections["users"].FindOne(
		ctx,
		bson.M{"tg_user_id": userID},
	)
	if res.Err() == nil {
		if b, err := res.DecodeBytes(); err != nil {
			if b.String() == "" && err.Error() == NoDocumentsError {
				return nil, nil
			}

			return nil, err
		}

		data := new(domain.User)
		err := res.Decode(data)
		if err != nil {
			return nil, err
		}

		return data.Contact, nil
	}

	return nil, res.Err()
}
