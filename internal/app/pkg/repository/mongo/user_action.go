package mongo

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"insurance_bot/internal/app/pkg/domain"
)

func (s *Storage) CreateUserAction(ctx context.Context, userID int, actionType domain.UserActionType) (string, error) {
	contact, err := s.GetContact(ctx, userID)
	if err != nil {
		return "", err
	}

	action := &domain.UserAction{
		UserID:     userID,
		Contact:    contact,
		ActionType: actionType,
		Step:       1,
		Properties: nil,
		Created:    time.Now(),
		Updated:    time.Now(),
	}

	if action.ActionType == domain.InsuranceUserActionType {
		action.Properties = s.userActionProperties
	}

	res := s.collections["actions"].FindOneAndUpdate(
		ctx,
		bson.M{"tg_user_id": userID},
		bson.M{"$set": action},
		options.FindOneAndUpdate().SetReturnDocument(options.After),
		options.FindOneAndUpdate().SetUpsert(true),
	)
	if res.Err() != nil {
		return "", res.Err()
	}

	data := new(domain.UserAction)
	err = res.Decode(data)
	if err != nil {
		return "", err
	}

	return data.ID.Hex(), nil
}

func (s *Storage) UpdateUserActionProperty(ctx context.Context, userID int, property string) (*domain.UserAction, error) {
	res := s.collections["actions"].FindOneAndUpdate(
		ctx,
		bson.M{"tg_user_id": userID, "properties.name": property},
		bson.M{"$bit": bson.M{"properties.$.is_selected": bson.M{"xor": 1}}, "$set": bson.M{"updated": time.Now()}},
		options.FindOneAndUpdate().SetReturnDocument(options.After),
	)
	if res.Err() != nil {
		return nil, res.Err()
	}

	data := new(domain.UserAction)
	err := res.Decode(data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (s *Storage) IncrementUserActionStep(ctx context.Context, userID int) (*domain.UserAction, error) {
	res := s.collections["actions"].FindOneAndUpdate(
		ctx,
		bson.M{"tg_user_id": userID},
		bson.M{"$inc": bson.M{"step": 1}, "$set": bson.M{"updated": time.Now()}},
		options.FindOneAndUpdate().SetReturnDocument(options.After),
	)
	if res.Err() != nil {
		return nil, res.Err()
	}

	data := new(domain.UserAction)
	err := res.Decode(data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (s *Storage) HasSelectedUserActionProperty(ctx context.Context, userID int) (bool, error) {
	res := s.collections["actions"].FindOne(
		ctx,
		bson.M{"tg_user_id": userID, "properties.is_selected": 1},
	)
	if res.Err() == nil {
		if b, err := res.DecodeBytes(); err != nil {
			if b.String() == "" && err.Error() == NoDocumentsError {
				return false, nil
			}

			return false, err
		}

		data := new(domain.UserAction)
		err := res.Decode(data)
		if err != nil {
			return false, err
		}

		return true, nil
	}

	return false, res.Err()
}

func (s *Storage) GetUserActionProperties(customFieldValues map[domain.LeadCustomFieldType][]string) {
	props := make([]*domain.UserActionProperty, 0)
	for t, l := range customFieldValues {
		for _, v := range l {
			props = append(props, &domain.UserActionProperty{
				Name:       v,
				Type:       string(t),
				IsSelected: 0,
			})
		}
	}

	s.userActionProperties = props
}
