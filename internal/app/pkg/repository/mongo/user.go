package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"

	"insurance_bot/internal/app/pkg/domain"
)

func (s *Storage) GetAllUsers(ctx context.Context) ([]*domain.User, error) {
	cur, err := s.collections["users"].Find(
		ctx,
		bson.M{},
	)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	users := make([]*domain.User, 0)
	for cur.Next(ctx) {
		user := new(domain.User)
		err := cur.Decode(user)
		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	return users, nil
}
