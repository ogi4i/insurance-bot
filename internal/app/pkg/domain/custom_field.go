package domain

type CustomField struct {
	ID       int
	Name     string
	Values   []string
	IsSystem bool
}
