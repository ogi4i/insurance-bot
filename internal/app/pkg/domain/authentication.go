package domain

type Authentication struct {
	Username string `yaml:"username" validator:"required"`
	Password string `yaml:"password" validator:"required"`
}
