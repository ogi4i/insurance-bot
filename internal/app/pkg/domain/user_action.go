package domain

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type (
	UserAction struct {
		ID         primitive.ObjectID    `bson:"_id,omitempty"`
		UserID     int                   `bson:"tg_user_id"`
		Contact    *Contact              `bson:"contact"`
		ActionType UserActionType        `bson:"action"`
		Step       int                   `bson:"step"`
		Properties []*UserActionProperty `bson:"properties"`
		Created    time.Time             `bson:"created"`
		Updated    time.Time             `bson:"updated"`
	}

	UserActionProperty struct {
		Name       string `bson:"name"`
		Type       string `bson:"type"`
		IsSelected int    `bson:"is_selected"`
	}

	UserActionType string
)

const (
	InsuranceUserActionType UserActionType = "insurance"
)
