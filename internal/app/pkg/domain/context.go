package domain

type ContextKey string

const (
	UUIDContextKey ContextKey = "uuid"
)
