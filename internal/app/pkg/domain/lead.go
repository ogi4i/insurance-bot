package domain

import (
	"time"

	"github.com/ogi4i/amocrm-client"
)

type (
	Lead struct {
		ID                int
		Name              string
		ResponsibleUserID int
		CreatedBy         int
		CreatedAt         time.Time
		UpdatedAt         time.Time
		AccountID         int
		IsDeleted         bool
		MainContactID     int
		GroupID           int
		ClosedAt          time.Time
		ClosestTaskAt     time.Time
		Tags              []*amocrm.Tag
		CustomFields      map[LeadCustomFieldType][]string
		Contacts          []int
		StatusID          int
		Sale              int
		PipelineID        int
	}

	LeadCustomFieldType string

	LeadStatus string
)

const (
	dateTimeLayout = "2006-01-02 15:04:05"

	InsuranceTypeLeadCustomFieldType  LeadCustomFieldType = "insurance_type"
	ExpiryDateLeadCustomFieldType     LeadCustomFieldType = "expire_date"
	IsExtendedLeadCustomFieldType     LeadCustomFieldType = "is_extended"
	CarLeadCustomFieldType            LeadCustomFieldType = "car"
	DeliveryMethodLeadCustomFieldType LeadCustomFieldType = "delivery_method"
	PaymentMethodLeadCustomFieldType  LeadCustomFieldType = "payment_method"

	ClientHasPolicyLeadstatus LeadStatus = "client_has_policy"
	SuccessLeadStatus         LeadStatus = "success"
	ApprovedLeadStatus        LeadStatus = "approved"

	TrueString = "1"
)

func (l *Lead) HasInsuranceTypes(insuranceTypes ...string) bool {
	if values, ok := l.CustomFields[InsuranceTypeLeadCustomFieldType]; ok {
		if len(insuranceTypes) == 0 {
			return true
		}

		for _, s := range values {
			for _, i := range insuranceTypes {
				if i == s {
					return true
				}
			}
		}
	}
	return false
}

func (l *Lead) IsExpired() bool {
	if values, ok := l.CustomFields[ExpiryDateLeadCustomFieldType]; ok {
		t, err := time.Parse(dateTimeLayout, values[0])
		if err != nil {
			return false
		}

		if t.IsZero() {
			return false
		}

		if time.Until(t) < 0 {
			return true
		}
	}
	return false
}

func (l *Lead) ExpiresSoon(until time.Duration) bool {
	if values, ok := l.CustomFields[ExpiryDateLeadCustomFieldType]; ok {
		t, err := time.Parse(dateTimeLayout, values[0])
		if err != nil {
			return false
		}

		if t.IsZero() {
			return false
		}

		if time.Until(t) < until {
			return true
		}
	}
	return false
}

func (l *Lead) IsExtended() bool {
	if values, ok := l.CustomFields[IsExtendedLeadCustomFieldType]; ok {
		switch values[0] {
		case TrueString:
			return true
		default:
			return false
		}
	}
	return false
}

func (l *Lead) HasCar() bool {
	if _, ok := l.CustomFields[CarLeadCustomFieldType]; ok {
		return true
	}
	return false
}

func (l *Lead) HasDeliveryMethod(method string) bool {
	if values, ok := l.CustomFields[DeliveryMethodLeadCustomFieldType]; ok {
		if values[0] == method {
			return true
		}
	}
	return false
}

func (l *Lead) HasExpiryDate() bool {
	if values, ok := l.CustomFields[ExpiryDateLeadCustomFieldType]; ok {
		_, err := time.Parse(dateTimeLayout, values[0])
		return err == nil
	}
	return false
}

func (l *Lead) PrintExpiryDate(layout string) string {
	if values, ok := l.CustomFields[ExpiryDateLeadCustomFieldType]; ok {
		t, err := time.Parse(dateTimeLayout, values[0])
		if err != nil {
			return ""
		}
		return t.Format(layout)
	}
	return ""
}
