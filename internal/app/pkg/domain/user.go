package domain

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	ID                       primitive.ObjectID `json:"-" bson:"_id"`
	UserID                   int                `json:"tg_user_id" bson:"tg_user_id"`
	Contact                  *Contact           `json:"contact" bson:"contact"`
	NotificationsSentOnLeads map[string][]int   `json:"-" bson:"notifications_sent_on_leads,omitempty"`
}

func ChannelOfUsersToSlice(ch chan *User) []*User {
	s := make([]*User, 0)
	for c := range ch {
		s = append(s, c)
	}
	return s
}
