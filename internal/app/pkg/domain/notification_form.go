package domain

type (
	NotificationPostFormField string

	NotificationFormat string

	NotificationTemplateType string
)

const (
	NotificationFormTemplateType        NotificationTemplateType = "notification_form"
	NotificationFormEmptyTemplateType   NotificationTemplateType = "notification_form_empty"
	NotificationFormConfirmTemplateType NotificationTemplateType = "notification_form_confirm"
	NotificationFormSuccessTemplateType NotificationTemplateType = "notification_form_success"
	NotificationFormErrorTemplateType   NotificationTemplateType = "notification_form_error"

	TextAreaPostFormField      NotificationPostFormField = "textarea"
	InsuranceTypePostFormField NotificationPostFormField = "insurance_type"
	FormatPostFormField        NotificationPostFormField = "format"

	MarkdownNotificationFormat NotificationFormat = "markdown"
)
