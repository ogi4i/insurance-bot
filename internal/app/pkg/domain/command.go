package domain

type Command string

const (
	StartCommand Command = "start"
	HelpCommand  Command = "help"
)
