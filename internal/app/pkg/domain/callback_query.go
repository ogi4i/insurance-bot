package domain

type CallbackQuery string

const (
	NewInsuranceCallbackQuery         CallbackQuery = "new_insurance"
	NewInsuranceNextStepCallbackQuery CallbackQuery = "new_insurance_next_step"
	ExtendInsuranceCallbackQuery      CallbackQuery = "insurance_extend"
	DownloadInsuranceCallbackQuery    CallbackQuery = "insurance_download"
	ManualCallbackQuery               CallbackQuery = "manual"
)
