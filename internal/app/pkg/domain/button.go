package domain

type Button string

const (
	ExtendInsuranceButton   Button = "extend_insurance_button"
	DownloadInsuranceButton Button = "download_insurance_button"
	InsuranceListButton     Button = "insurance_list_button"
	InsuranceCaseButton     Button = "insurance_case_button"
	NewInsuranceButton      Button = "new_insurance_button"
	PromocodeButton         Button = "promocode_button"
	ContactsButton          Button = "contacts_button"
	HelpButton              Button = "help_button"
	SendPhoneNumberButton   Button = "send_phone_number_button"
)
