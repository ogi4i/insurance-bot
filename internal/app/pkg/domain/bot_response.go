package domain

type (
	BotResponse struct {
		IsMarkdown bool   `yaml:"markdown" validator:"omitempty"`
		Text       string `yaml:"text" validator:"required"`
	}

	BotResponseKey string
)

const (
	ManualSuccessKey                   BotResponseKey = "manual_success"
	ChoosePaymentMethodKey             BotResponseKey = "choose_payment_method"
	ChooseDeliveryMethodKey            BotResponseKey = "choose_delivery_method"
	FlowSuccessKey                     BotResponseKey = "flow_success"
	ChooseInsuranceTypeKey             BotResponseKey = "choose_insurance_type"
	AuthorizeRequestKey                BotResponseKey = "authorize_request"
	GreetingKey                        BotResponseKey = "greeting"
	InsuranceTypeItemKey               BotResponseKey = "insurance_type_item"
	CarItemKey                         BotResponseKey = "car_item"
	SaleItemKey                        BotResponseKey = "sale_item"
	ExpiryDateItemKey                  BotResponseKey = "expiry_date_item"
	InsuranceExpirationNotificationKey BotResponseKey = "insurance_expiration_notification"
	NewInsurancePDFNotificationKey     BotResponseKey = "new_insurance_pdf_notification"
	PromocodeKey                       BotResponseKey = "promocode"
	HelpKey                            BotResponseKey = "help"
	ContactsKey                        BotResponseKey = "contacts"
	UnknownCommandKey                  BotResponseKey = "unknown_command"
	UnknownPhoneNumberKey              BotResponseKey = "unknown_phone_number"
	ChooseManualKey                    BotResponseKey = "choose_manual"
	NoInsuranceKey                     BotResponseKey = "no_insurance"
)
