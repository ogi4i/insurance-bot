package domain

type Error string

func (e Error) Error() string {
	return string(e)
}

var (
	ErrInternalServerError Error = "internal_server_error"
)
