package domain

type (
	Contact struct {
		ID          int                          `json:"id" bson:"id"`
		Name        string                       `json:"name" bson:"name"`
		PhoneNumber string                       `json:"phone_number" bson:"phone_number"`
		Properties  map[ContactProperty][]string `json:"-" bson:"-"`
	}

	ContactProperty string
)

const (
	PromocodeContactProperty   ContactProperty = "promocode"
	PhoneNumberContactProperty ContactProperty = "phone_number"
)
