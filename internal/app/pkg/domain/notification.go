package domain

import (
	"net/http"

	"gopkg.in/go-playground/validator.v9"
)

type Notification struct {
	UUID           string   `json:"uuid" validator:"required"`
	Text           string   `json:"text" validator:"required"`
	InsuranceTypes []string `json:"insurance_types" validator:"required,dive,required"`
	IsMarkdown     bool     `json:"is_markdown" validator:"omitempty"`
	IsAll          bool     `json:"is_all" validator:"omitempty"`
	Users          []*User  `json:"users" validator:"required,dive,required"`
}

func (n *Notification) Bind(r *http.Request) error {
	validator := validator.New()
	if err := validator.Struct(n); err != nil {
		return err
	}
	return nil
}
