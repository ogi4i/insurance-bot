package config

import "time"

type (
	Database struct {
		DSN         string        `yaml:"dsn" validator:"required,mongodsn"`
		Name        string        `yaml:"name" validator:"required,alphanum"`
		Collections []*Collection `yaml:"collections" validator:"required"`
		Timeout     time.Duration `yaml:"timeout" validator:"required,min=1"`
	}

	Collection struct {
		Resource string               `yaml:"resource" validator:"required,alphanum"`
		Name     string               `yaml:"name" validator:"required,alphanum"`
		Indexes  []*CollectionIndexes `yaml:"indexes" validator:"required"`
	}

	CollectionIndexes struct {
		Name   string `yaml:"name" validator:"required,alphanum"`
		Unique bool   `yaml:"unique" validator:"required"`
		Field  string `yaml:"field" validator:"required,alphanum"`
	}
)
