package config

import "time"

type TimeZone struct {
	*time.Location
}

func (tz *TimeZone) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var timezone string
	err := unmarshal(&timezone)
	if err != nil {
		return err
	}

	location, err := time.LoadLocation(timezone)
	if err != nil {
		return err
	}

	*tz = TimeZone{Location: location}
	return nil
}

func (tz *TimeZone) ToLocation() *time.Location {
	return tz.Location
}
