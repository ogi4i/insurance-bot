package config

import "time"

type Telegram struct {
	Token         string        `yaml:"token" validator:"required"`
	WebHookDomain string        `yaml:"web_hook_domain" validator:"required"`
	Timeout       time.Duration `yaml:"timeout" validator:"required"`
	RetryCount    int           `yaml:"retry_count" validator:"required"`
	RetryBackoff  time.Duration `yaml:"retry_backoff" validator:"required"`
}
