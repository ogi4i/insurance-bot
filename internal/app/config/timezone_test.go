package config

import (
	"testing"
	"time"

	"gopkg.in/yaml.v2"

	"github.com/stretchr/testify/assert"
)

func TestTimeZone_UnmarshalYAML(t *testing.T) {

	type TestTimezoneConfig struct {
		Timezone *TimeZone `yaml:"timezone"`
	}

	var (
		validTimeZone   = []byte("timezone: Europe/Moscow")
		invalidTimeZone = []byte("timezone: Europe/Europe")
	)

	t.Run("success_europe_moscow", func(t *testing.T) {
		loc, _ := time.LoadLocation("Europe/Moscow")
		expected := &TestTimezoneConfig{Timezone: &TimeZone{Location: loc}}

		actual := &TestTimezoneConfig{}
		err := yaml.Unmarshal(validTimeZone, actual)

		assert.NoError(t, err)
		assert.Equal(t, expected, actual)
	})

	t.Run("failed_unknown_location", func(t *testing.T) {
		actual := &TestTimezoneConfig{}
		err := yaml.Unmarshal(invalidTimeZone, actual)

		assert.EqualError(t, err, "unknown time zone Europe/Europe")
	})
}

func TestTimeZone_ToLocation(t *testing.T) {

	t.Run("success_europe_moscow", func(t *testing.T) {
		loc, _ := time.LoadLocation("Europe/Moscow")
		tz := &TimeZone{Location: loc}

		assert.Equal(t, loc, tz.ToLocation())
	})
}
