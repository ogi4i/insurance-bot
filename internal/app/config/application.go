package config

import (
	"insurance_bot/internal/app/pkg/domain"
	"time"
)

type (
	Application struct {
		TimeZone          *TimeZone                                     `yaml:"timezone" validator:"required"`
		NotificationForm  *NotificationForm                             `yaml:"notification_form" validator:"required"`
		NewInsurance      *NewInsurance                                 `yaml:"new_insurance" validator:"required"`
		ExtendInsurance   *ExtendInsurance                              `yaml:"extend_insurance" validator:"required"`
		PromoCode         *PromoCode                                    `yaml:"promocode" validator:"required"`
		Manual            *Manual                                       `yaml:"manual" validator:"required"`
		DownloadInsurance *DownloadInsurance                            `yaml:"download_insurance" validator:"required"`
		Notifications     *Notifications                                `yaml:"notifications" validator:"required"`
		BotResponses      map[domain.BotResponseKey]*domain.BotResponse `yaml:"bot_responses" validator:"required,len=20,dive,keys,oneof=manual_success choose_payment_method choose_delivery_method flow_success choose_insurance_type authorize_request greeting insurance_type_item car_item sale_item expiry_date_item insurance_expiration_notification promocode help contacts unknown_command unknown_phone_number choose_manual car_insurance_item insurance_with_expiry_item insurance_without_expiry_item new_insurance_pdf_notification car_insurance_without_expiry_item no_insurance,endkeys,required"`
		BotButtons        map[domain.Button]string                      `yaml:"bot_buttons" validator:"required,len=9,dive,keys,oneof=extend_insurance_button download_insurance_button insurance_list_button insurance_case_button new_insurance_button promocode_button contacts_button help_button send_phone_number_button,endkeys,required"`
	}

	NewInsurance struct {
		Enabled bool `yaml:"enabled" validator:"-"`
	}

	ExtendInsurance struct {
		Enabled                  bool          `yaml:"enabled" validator:"omitempty"`
		ShowButtonTillExpiration time.Duration `yaml:"show_button_till_expiration" validator:"required"`
		NotifyAt                 string        `yaml:"notify_at" validator:"required"`
	}

	PromoCode struct {
		Enabled bool   `yaml:"enabled" validator:"omitempty"`
		Pattern string `yaml:"pattern" validator:"required"`
		Charset string `yaml:"charset" validator:"required"`
	}

	Manual struct {
		Enabled     bool           `yaml:"enabled" validator:"omitempty"`
		ManualTypes []*ManualTypes `yaml:"manual_types" validator:"required"`
	}

	ManualTypes struct {
		Name      string   `yaml:"name" validator:"required"`
		Documents []string `yaml:"documents" validator:"required"`
	}

	NotificationForm struct {
		Enabled        bool                     `yaml:"enabled" validator:"omitempty"`
		Authentication []*domain.Authentication `yaml:"authentication" validator:"required"`
	}

	Notifications struct {
		ExpirationNotifications []int  `yaml:"expiration_notifications" validator:"required"`
		PDFNotification         uint64 `yaml:"pdf_notification" validator:"required"`
	}

	DownloadInsurance struct {
		Enabled bool `yaml:"enabled" validator:"omitempty"`
	}
)
