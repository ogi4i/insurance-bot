package config

import (
	"insurance_bot/internal/app/pkg/domain"
)

type (
	AmoCRM struct {
		Login        string         `yaml:"login" validator:"required"`
		APIKey       string         `yaml:"api_key" validator:"required"`
		Domain       string         `yaml:"domain" validator:"required"`
		RetryCount   int            `yaml:"retry_count" validator:"required"`
		Pipeline     *Pipeline      `yaml:"pipeline" validator:"required"`
		TaskTypes    map[int]string `yaml:"task_types" validator:"required,unique,dive,oneof=call meeting email control nps delivery extension replacement registration estimate difficulties accompaniment add_contact hz"`
		CustomFields *CustomFields  `yaml:"custom_fields" validator:"required"`
	}

	Pipeline struct {
		ID       int                       `yaml:"id" validator:"required"`
		Statuses map[int]domain.LeadStatus `yaml:"statuses" validator:"required,unique,dive,oneof=success rejected requested client_has_policy estimation_sent waiting_for_estimate approved"`
	}

	CustomFields struct {
		Leads    map[int]*LeadsCustomFields     `yaml:"leads" validator:"required,len=8,dive,required"`
		Contacts map[int]domain.ContactProperty `yaml:"contacts" validator:"required,unique,len=7,dive,oneof=position phone_number email im source date_of_birth promocode"`
	}

	LeadsCustomFields struct {
		Type                domain.LeadCustomFieldType `yaml:"type" validator:"required,oneof=insurance_type car payment_method delivery_method expire_date partner documents_printed is_extended"`
		IsIncludedInNewLead bool                       `yaml:"is_included_in_new_lead" validator:"omitempty"`
	}
)
