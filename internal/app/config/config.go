package config

import (
	"io/ioutil"

	"gopkg.in/go-playground/validator.v9"
	"gopkg.in/yaml.v2"

	valid "insurance_bot/pkg/validator"
)

const (
	configFile = "config.yml"
)

type Config struct {
	AmoCRM      *AmoCRM      `yaml:"crm" validator:"required"`
	Telegram    *Telegram    `yaml:"telegram" validator:"required"`
	Database    *Database    `yaml:"database" validator:"required"`
	Application *Application `yaml:"application" validator:"required"`
}

func ParseConfig() (*Config, error) {
	file, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, err
	}

	config := new(Config)
	if err := yaml.Unmarshal(file, &config); err != nil {
		return nil, err
	}

	validator := validator.New()
	if err := valid.RegisterValidators(validator); err != nil {
		return nil, err
	}
	if err := validator.Struct(config); err != nil {
		return nil, err
	}

	return config, nil
}
