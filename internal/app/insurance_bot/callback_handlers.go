package insurance_bot

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/sync/errgroup"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/pkg/domain"
	"insurance_bot/internal/app/pkg/telegram_bot"
)

const (
	tempDirSuffix = "_insurance_bot"
)

func (i *Implementation) newInsuranceNextStepCallbackHandler(ctx context.Context, update *tgbotapi.Update) error {
	if ok, err := i.storage.HasSelectedUserActionProperty(ctx, update.CallbackQuery.From.ID); ok {
		var userAction *domain.UserAction
		userAction, err = i.storage.IncrementUserActionStep(ctx, update.CallbackQuery.From.ID)
		if err != nil {
			return err
		}

		switch userAction.Step {
		case 2:
			return i.bot.SendMessageWithRetry(
				ctx,
				telegram_bot.EditMessage(
					update.CallbackQuery.Message.Chat.ID,
					update.CallbackQuery.Message.MessageID,
					i.config.BotResponses[domain.ChoosePaymentMethodKey].Text,
					i.NewSortedKeyboardWithCustomFieldValues(domain.PaymentMethodLeadCustomFieldType, domain.NewInsuranceCallbackQuery),
					i.config.BotResponses[domain.ChoosePaymentMethodKey].IsMarkdown,
				),
			)
		case 3:
			return i.bot.SendMessageWithRetry(
				ctx,
				telegram_bot.EditMessage(
					update.CallbackQuery.Message.Chat.ID,
					update.CallbackQuery.Message.MessageID,
					i.config.BotResponses[domain.ChooseDeliveryMethodKey].Text,
					i.NewSortedKeyboardWithCustomFieldValues(domain.DeliveryMethodLeadCustomFieldType, domain.NewInsuranceCallbackQuery),
					i.config.BotResponses[domain.ChooseDeliveryMethodKey].IsMarkdown,
				),
			)
		case 4:
			if i.config.NewInsurance.Enabled {
				err = i.crm.CreateNewLead(ctx, userAction)
				if err != nil {
					return err
				}
			}

			return i.bot.SendMessageWithRetry(
				ctx,
				telegram_bot.EditMessage(
					update.CallbackQuery.Message.Chat.ID,
					update.CallbackQuery.Message.MessageID,
					i.config.BotResponses[domain.FlowSuccessKey].Text,
					nil,
					i.config.BotResponses[domain.FlowSuccessKey].IsMarkdown,
				),
			)
		default:
			return nil
		}
	} else if err != nil {
		return err
	}

	return i.bot.AnswerCallback(
		tgbotapi.NewCallbackWithAlert(
			update.CallbackQuery.ID,
			i.config.BotResponses[domain.ChooseInsuranceTypeKey].Text,
		),
	)
}

func (i *Implementation) defaultCallbackHandler(ctx context.Context, update *tgbotapi.Update) error {
	callbackData := update.CallbackQuery.Data
	if strings.HasPrefix(callbackData, string(domain.NewInsuranceCallbackQuery)) {
		return i.newInsuranceCallbackHandler(ctx, update)
	}

	if strings.HasPrefix(callbackData, string(domain.NewInsuranceNextStepCallbackQuery)) {
		return i.newInsuranceNextStepCallbackHandler(ctx, update)
	}

	if strings.HasPrefix(callbackData, string(domain.ExtendInsuranceCallbackQuery)) {
		return i.extendInsuranceCallbackHandler(ctx, update)
	}

	if strings.HasPrefix(callbackData, string(domain.ManualCallbackQuery)) {
		return i.manualCallbackHandler(ctx, update)
	}

	if strings.HasPrefix(callbackData, string(domain.DownloadInsuranceCallbackQuery)) {
		return i.downloadInsuranceCallbackHandler(ctx, update)
	}

	return nil
}

func (i *Implementation) manualCallbackHandler(ctx context.Context, update *tgbotapi.Update) error {
	manualType := strings.Split(update.CallbackQuery.Data, ":")[1]

	if i.config.Manual.Enabled {
		g, _ := errgroup.WithContext(ctx)
		for _, m := range i.config.Manual.ManualTypes {
			if m.Name == manualType {
				for _, d := range m.Documents {
					g.Go(func() error {
						return i.bot.SendMessageWithRetry(
							ctx,
							telegram_bot.NewDocument(update.CallbackQuery.Message.Chat.ID, d),
						)
					})
				}
			}
		}

		if err := g.Wait(); err != nil {
			return err
		}
	}

	return i.bot.SendMessageWithRetry(
		ctx,
		telegram_bot.EditMessage(
			update.CallbackQuery.Message.Chat.ID,
			update.CallbackQuery.Message.MessageID,
			fmt.Sprintf(i.config.BotResponses[domain.ManualSuccessKey].Text, manualType),
			nil,
			i.config.BotResponses[domain.ManualSuccessKey].IsMarkdown,
		),
	)
}

func (i *Implementation) newInsuranceCallbackHandler(ctx context.Context, update *tgbotapi.Update) error {
	property := strings.Split(update.CallbackQuery.Data, ":")[1]
	userAction, err := i.storage.UpdateUserActionProperty(ctx, update.CallbackQuery.From.ID, property)
	if err != nil {
		return err
	}

	if userAction.Step == 1 {
		return i.bot.SendMessageWithRetry(
			ctx,
			telegram_bot.EditMessage(
				update.CallbackQuery.Message.Chat.ID,
				update.CallbackQuery.Message.MessageID,
				i.config.BotResponses[domain.ChooseInsuranceTypeKey].Text,
				i.NewInsuranceTypeKeyboard(userAction.Properties),
				i.config.BotResponses[domain.ChooseInsuranceTypeKey].IsMarkdown,
			),
		)
	}

	return i.newInsuranceNextStepCallbackHandler(ctx, update)
}

func (i *Implementation) extendInsuranceCallbackHandler(ctx context.Context, update *tgbotapi.Update) error {
	leadID, err := strconv.ParseInt(strings.Split(update.CallbackQuery.Data, ":")[1], 10, 64)
	if err != nil {
		return err
	}

	lead, err := i.crm.GetLeadByLeadID(ctx, int(leadID))
	if err != nil {
		return err
	}

	if i.config.ExtendInsurance.Enabled {
		if err := i.crm.CreateNewLead(ctx, lead); err != nil {
			return err
		}

		if err := i.crm.CompleteExtensionTask(ctx, int(leadID)); err != nil {
			return err
		}

		if err := i.crm.AddExtensionFlagToLead(ctx, lead); err != nil {
			return err
		}
	}

	return i.bot.SendMessageWithRetry(
		ctx,
		telegram_bot.EditMessage(
			update.CallbackQuery.Message.Chat.ID,
			update.CallbackQuery.Message.MessageID,
			i.config.BotResponses[domain.FlowSuccessKey].Text,
			nil,
			i.config.BotResponses[domain.FlowSuccessKey].IsMarkdown,
		),
	)
}

func (i *Implementation) downloadInsuranceCallbackHandler(ctx context.Context, update *tgbotapi.Update) error {
	leadID, err := strconv.ParseInt(strings.Split(update.CallbackQuery.Data, ":")[1], 10, 64)
	if err != nil {
		return err
	}

	lead, err := i.crm.GetLeadByLeadID(ctx, int(leadID))
	if err != nil {
		return err
	}

	if files, ok := i.tempFiles[lead.ID]; ok {
		err = i.bot.SendMessageWithRetry(
			ctx,
			telegram_bot.EditMessage(
				update.CallbackQuery.Message.Chat.ID,
				update.CallbackQuery.Message.MessageID,
				i.GetResponseFromLead(lead),
				nil,
				true,
			),
		)
		if err != nil {
			return err
		}

		g, ctx := errgroup.WithContext(ctx)
		files := files
		g.Go(func() error {
			return i.bot.SendDocuments(ctx, update.CallbackQuery.Message.Chat.ID, files)
		})

		err = g.Wait()
		if err != nil {
			return err
		}
	}

	attachments, err := i.crm.GetPDFAttachmentsFromLead(ctx, lead.ID)
	if err != nil {
		return err
	}

	if len(attachments) != 0 {
		if err := i.bot.SendMessageWithRetry(
			ctx,
			telegram_bot.EditMessage(
				update.CallbackQuery.Message.Chat.ID,
				update.CallbackQuery.Message.MessageID,
				i.GetResponseFromLead(lead),
				nil,
				true,
			),
		); err != nil {
			return err
		}

		filepaths := make(chan string, len(attachments))
		gAttachments, ctx := errgroup.WithContext(ctx)
		gFiles, ctx := errgroup.WithContext(ctx)
		for _, a := range attachments {
			file := *a
			userID := update.CallbackQuery.Message.Chat.ID
			gAttachments.Go(func() error {
				return i.bot.SendMessageWithRetry(
					ctx,
					telegram_bot.NewDocument(userID, file),
				)
			})

			fileP := a
			gFiles.Go(func() error {
				filepath, err := writeFileToTempDir(tempDirSuffix, lead.ID, fileP)
				if err != nil {
					return err
				}
				filepaths <- filepath

				return nil
			})
		}

		if err := gAttachments.Wait(); err != nil {
			return err
		}

		if err := gFiles.Wait(); err != nil {
			return err
		}
		close(filepaths)

		i.FillTempFilesFromChannel(lead.ID, filepaths)
	}

	return nil
}
