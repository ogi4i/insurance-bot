package insurance_bot

import (
	"context"
	"html/template"
	"io"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/ogi4i/amocrm-client"
	"github.com/patrickmn/go-cache"

	"insurance_bot/internal/app/config"
	"insurance_bot/internal/app/pkg/domain"
)

type (
	Service interface {
		IsAuthenticated(ctx context.Context, tgUserID int) (*domain.Contact, bool)
		PrepareUserActionProperties(customFields ...domain.LeadCustomFieldType)

		HandleUpdate(ctx context.Context, update *tgbotapi.Update) error

		GetForm(w io.Writer) error
		NewNotification(ctx context.Context, w io.Writer, text string, insuranceTypes []string, format string) error
		ConfirmNotification(w io.Writer, id string) error
		GetErrorForm(w io.Writer) error
		DoesUUIDExist(uuid string) bool

		ClearTempDir()
	}

	Storage interface {
		BindUserIDToContact(ctx context.Context, userID int, contact *domain.Contact) (*domain.User, error)
		GetContact(ctx context.Context, userID int) (*domain.Contact, error)
		GetAllUsers(ctx context.Context) ([]*domain.User, error)

		CreateUserAction(ctx context.Context, userID int, actionType domain.UserActionType) (string, error)
		GetUserActionProperties(customFieldValues map[domain.LeadCustomFieldType][]string)
		UpdateUserActionProperty(ctx context.Context, userID int, property string) (*domain.UserAction, error)
		IncrementUserActionStep(ctx context.Context, userID int) (*domain.UserAction, error)
		HasSelectedUserActionProperty(ctx context.Context, userID int) (bool, error)

		InsertNotifications(ctx context.Context, until int, amoUserID int, leadIds []int) error
	}

	CRM interface {
		SearchContact(ctx context.Context, params *amocrm.ContactRequestParams) (*domain.Contact, error)
		AddPromocode(ctx context.Context, contact *domain.Contact, promocode string) error

		GetLeadsByContactIDs(ctx context.Context, contactIds []int) (map[int][]*domain.Lead, error)
		GetLeadByLeadID(ctx context.Context, leadID int) (*domain.Lead, error)
		GetPDFAttachmentsFromLead(ctx context.Context, leadID int) ([]*tgbotapi.FileBytes, error)
		CreateNewLead(ctx context.Context, data interface{}) error
		ChangeLeadStatusTo(ctx context.Context, leadID int, status domain.LeadStatus) error
		LeadHasPDFAttachments(ctx context.Context, leadID int) bool
		AddExtensionFlagToLead(ctx context.Context, lead *domain.Lead) error
		IsLeadInOneOfStatuses(lead *domain.Lead, status ...domain.LeadStatus) bool
		GetLeadCustomFields(customFields ...domain.LeadCustomFieldType) map[domain.LeadCustomFieldType][]string

		CompleteExtensionTask(ctx context.Context, leadID int) error
	}

	Bot interface {
		AnswerCallback(callback tgbotapi.CallbackConfig) error
		SendMessageWithRetry(ctx context.Context, msg tgbotapi.Chattable) error
		SendDocuments(ctx context.Context, tgUserID int64, files []string) error
	}

	Implementation struct {
		storage   Storage
		crm       CRM
		bot       Bot
		config    *config.Application
		keyboards map[Keyboard]*tgbotapi.ReplyKeyboardMarkup
		templates *template.Template
		cache     *cache.Cache
		tempFiles map[int][]string
	}
)

func NewInsuranceBotService(storage Storage, client CRM, bot Bot, cache *cache.Cache, templates *template.Template, appConfig *config.Application) *Implementation {
	impl := &Implementation{
		storage:   storage,
		crm:       client,
		bot:       bot,
		cache:     cache,
		templates: templates,
		config:    appConfig,
		keyboards: make(map[Keyboard]*tgbotapi.ReplyKeyboardMarkup),
		tempFiles: make(map[int][]string),
	}
	impl.initKeyboards()

	return impl
}

func (i *Implementation) PrepareUserActionProperties(customFields ...domain.LeadCustomFieldType) {
	i.storage.GetUserActionProperties(i.crm.GetLeadCustomFields(customFields...))
}

func (i *Implementation) IsAuthenticated(ctx context.Context, tgUserID int) (*domain.Contact, bool) {
	contact, err := i.storage.GetContact(ctx, tgUserID)
	if err != nil {
		return nil, false
	}

	if contact == nil {
		return nil, false
	}

	return contact, true
}

func (i *Implementation) ClearTempDir() {
	_ = os.RemoveAll(os.TempDir() + tempDirSuffix)
}
