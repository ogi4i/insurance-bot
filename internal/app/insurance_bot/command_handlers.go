package insurance_bot

import (
	"context"
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/ogi4i/amocrm-client"

	"insurance_bot/internal/app/pkg/domain"
	"insurance_bot/internal/app/pkg/telegram_bot"
)

func (i *Implementation) HandleUpdate(ctx context.Context, update *tgbotapi.Update) (err error) {
	if update.Message != nil {
		chatID := update.Message.Chat.ID
		userID := update.Message.From.ID
		if update.Message.IsCommand() {
			switch domain.Command(update.Message.Command()) {
			case domain.StartCommand:
				return i.startCommandHandler(ctx, chatID, userID)
			case domain.HelpCommand:
				return i.helpCommandHandler(ctx, chatID)
			default:
				return i.defaultCommandHandler(ctx, chatID)
			}
		} else {
			switch update.Message.Text {
			case i.config.BotButtons[domain.HelpButton]:
				return i.helpCommandHandler(ctx, chatID)
			case i.config.BotButtons[domain.ContactsButton]:
				return i.contactsHandler(ctx, chatID)
			case i.config.BotButtons[domain.InsuranceListButton]:
				return i.listInsuranceHandler(ctx, chatID, userID)
			case i.config.BotButtons[domain.InsuranceCaseButton]:
				return i.manualHandler(ctx, chatID, userID)
			case i.config.BotButtons[domain.NewInsuranceButton]:
				return i.newInsuranceHandler(ctx, chatID, userID)
			case i.config.BotButtons[domain.PromocodeButton]:
				return i.promoHandler(ctx, chatID, userID)
			default:
				return i.defaultTextHandler(ctx, update)
			}
		}
	}

	if update.CallbackQuery != nil {
		switch domain.CallbackQuery(update.CallbackQuery.Data) {
		case domain.NewInsuranceNextStepCallbackQuery:
			err = i.newInsuranceNextStepCallbackHandler(ctx, update)
			if err != nil {
				return err
			}
		default:
			err = i.defaultCallbackHandler(ctx, update)
			if err != nil {
				return err
			}
		}

		return i.bot.AnswerCallback(tgbotapi.NewCallback(update.CallbackQuery.ID, ""))
	}

	return nil
}

func (i *Implementation) startCommandHandler(ctx context.Context, chatID int64, userID int) error {
	contact, err := i.storage.GetContact(ctx, userID)
	if err != nil {
		return err
	}

	if contact != nil {
		amoContact, err := i.crm.SearchContact(ctx, &amocrm.ContactRequestParams{ID: []int{contact.ID}})
		if err != nil {
			return err
		}

		return i.bot.SendMessageWithRetry(ctx, i.AuthSuccessResponse(amoContact.Name, chatID))
	}

	return i.bot.SendMessageWithRetry(ctx, i.AuthResponse(chatID))
}

func (i *Implementation) defaultTextHandler(ctx context.Context, update *tgbotapi.Update) error {
	if update.Message.Contact != nil {
		return i.contactRecieveHandler(ctx, update)
	}

	return nil
}

func (i *Implementation) helpCommandHandler(ctx context.Context, chatID int64) error {
	return i.bot.SendMessageWithRetry(
		ctx,
		telegram_bot.NewMessage(
			chatID,
			i.config.BotResponses[domain.HelpKey].Text,
			nil,
			i.config.BotResponses[domain.HelpKey].IsMarkdown,
		),
	)
}

func (i *Implementation) defaultCommandHandler(ctx context.Context, chatID int64) error {
	return i.bot.SendMessageWithRetry(
		ctx,
		telegram_bot.NewMessage(
			chatID,
			i.config.BotResponses[domain.UnknownCommandKey].Text,
			nil,
			i.config.BotResponses[domain.UnknownCommandKey].IsMarkdown,
		),
	)
}

func (i *Implementation) contactRecieveHandler(ctx context.Context, update *tgbotapi.Update) error {
	contact, err := i.crm.SearchContact(ctx, &amocrm.ContactRequestParams{Query: update.Message.Contact.PhoneNumber})
	if err != nil {
		return err
	}

	if contact != nil {
		_, err = i.storage.BindUserIDToContact(ctx, update.Message.Contact.UserID, contact)
		if err != nil {
			return err
		}

		return i.bot.SendMessageWithRetry(ctx, i.AuthSuccessResponse(contact.Name, update.Message.Chat.ID))
	}

	return i.bot.SendMessageWithRetry(
		ctx,
		telegram_bot.NewMessage(
			update.Message.Chat.ID,
			fmt.Sprintf(i.config.BotResponses[domain.UnknownPhoneNumberKey].Text, update.Message.Contact.PhoneNumber),
			nil,
			i.config.BotResponses[domain.UnknownPhoneNumberKey].IsMarkdown,
		),
	)
}
