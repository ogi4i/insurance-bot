package insurance_bot

import (
	"context"
	"fmt"

	"github.com/ogi4i/amocrm-client"

	"golang.org/x/sync/errgroup"

	"insurance_bot/internal/app/pkg/domain"
	"insurance_bot/internal/app/pkg/telegram_bot"
)

func (i *Implementation) listInsuranceHandler(ctx context.Context, chatID int64, userID int) error {
	contact, ok := i.IsAuthenticated(ctx, userID)
	if !ok {
		return i.bot.SendMessageWithRetry(ctx, i.AuthResponse(chatID))
	}

	leads, err := i.crm.GetLeadsByContactIDs(ctx, []int{contact.ID})
	if err != nil {
		return err
	}

	filteredLeads := i.filterListInsuranceLeads(leads[contact.ID])
	if len(filteredLeads) == 0 {
		return i.bot.SendMessageWithRetry(
			ctx,
			telegram_bot.NewMessage(
				chatID,
				i.config.BotResponses[domain.NoInsuranceKey].Text,
				nil,
				i.config.BotResponses[domain.NoInsuranceKey].IsMarkdown,
			),
		)
	}

	g, ctx := errgroup.WithContext(ctx)
	for _, l := range filteredLeads {
		lead := l
		g.Go(func() error {
			return i.bot.SendMessageWithRetry(
				ctx,
				telegram_bot.NewMessage(
					chatID,
					i.GetResponseFromLead(lead),
					i.ListInsuranceKeyboard(ctx, lead),
					true,
				),
			)
		})
	}

	return g.Wait()
}

func (i *Implementation) newInsuranceHandler(ctx context.Context, chatID int64, userID int) error {
	if _, ok := i.IsAuthenticated(ctx, userID); ok {
		_, err := i.storage.CreateUserAction(ctx, userID, domain.InsuranceUserActionType)
		if err != nil {
			return err
		}

		return i.bot.SendMessageWithRetry(
			ctx,
			telegram_bot.NewMessage(
				chatID,
				i.config.BotResponses[domain.ChooseInsuranceTypeKey].Text,
				i.NewInsuranceTypeKeyboard(nil),
				i.config.BotResponses[domain.ChooseInsuranceTypeKey].IsMarkdown,
			),
		)
	}

	return i.bot.SendMessageWithRetry(ctx, i.AuthResponse(chatID))
}

func (i *Implementation) manualHandler(ctx context.Context, chatID int64, userID int) error {
	if _, ok := i.IsAuthenticated(ctx, userID); ok {
		return i.bot.SendMessageWithRetry(
			ctx,
			telegram_bot.NewMessage(
				chatID,
				i.config.BotResponses[domain.ChooseManualKey].Text,
				i.ManualKeyboard(),
				i.config.BotResponses[domain.ChooseManualKey].IsMarkdown,
			),
		)
	}

	return i.bot.SendMessageWithRetry(ctx, i.AuthResponse(chatID))
}

func (i *Implementation) promoHandler(ctx context.Context, chatID int64, userID int) error {
	if contact, ok := i.IsAuthenticated(ctx, userID); ok {
		amoContact, err := i.crm.SearchContact(ctx, &amocrm.ContactRequestParams{ID: []int{contact.ID}})
		if err != nil {
			return err
		}

		if promocodes, ok := amoContact.Properties[domain.PromocodeContactProperty]; ok {
			return i.bot.SendMessageWithRetry(
				ctx,
				telegram_bot.NewMessage(
					chatID,
					fmt.Sprintf(i.config.BotResponses[domain.PromocodeKey].Text, promocodes[0]),
					nil,
					i.config.BotResponses[domain.PromocodeKey].IsMarkdown,
				),
			)
		}

		promocode := i.GeneratePromoCode()
		if i.config.PromoCode.Enabled {
			if err := i.crm.AddPromocode(ctx, amoContact, promocode); err != nil {
				return err
			}
		}

		return i.bot.SendMessageWithRetry(
			ctx,
			telegram_bot.NewMessage(
				chatID,
				fmt.Sprintf(i.config.BotResponses[domain.PromocodeKey].Text, promocode),
				nil,
				i.config.BotResponses[domain.PromocodeKey].IsMarkdown,
			),
		)
	}

	return i.bot.SendMessageWithRetry(ctx, i.AuthResponse(chatID))
}

func (i *Implementation) contactsHandler(ctx context.Context, chatID int64) error {
	return i.bot.SendMessageWithRetry(
		ctx,
		telegram_bot.NewMessage(
			chatID,
			i.config.BotResponses[domain.ContactsKey].Text,
			nil,
			i.config.BotResponses[domain.ContactsKey].IsMarkdown,
		),
	)
}
