package insurance_bot

import (
	"context"
	"sort"
	"strconv"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/pkg/domain"
)

type Keyboard int8

const (
	UncheckedBox = "\xE2\x96\xAB "
	CheckMark    = "\xE2\x9C\x85 "

	MainMenuKeyboard Keyboard = iota
	AuthKeyboard
)

func (i *Implementation) ListInsuranceKeyboard(ctx context.Context, lead *domain.Lead) *tgbotapi.InlineKeyboardMarkup {
	// only show the extend button if expires soon and not already extended
	keyboard := &tgbotapi.InlineKeyboardMarkup{
		InlineKeyboard: [][]tgbotapi.InlineKeyboardButton{},
	}
	if i.config.ExtendInsurance.Enabled {
		if lead.ExpiresSoon(i.config.ExtendInsurance.ShowButtonTillExpiration*24*time.Hour) &&
			!lead.IsExtended() {
			keyboard.InlineKeyboard = append(keyboard.InlineKeyboard,
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData(
						i.config.BotButtons[domain.ExtendInsuranceButton],
						string(domain.ExtendInsuranceCallbackQuery)+":"+strconv.Itoa(lead.ID)),
				),
			)
		}
	}
	if i.config.DownloadInsurance.Enabled {
		if i.crm.LeadHasPDFAttachments(ctx, lead.ID) {
			keyboard.InlineKeyboard = append(keyboard.InlineKeyboard,
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData(
						i.config.BotButtons[domain.DownloadInsuranceButton],
						string(domain.DownloadInsuranceCallbackQuery)+":"+strconv.Itoa(lead.ID)),
				),
			)
		}
	}

	return keyboard
}

func (i *Implementation) NewInsuranceTypeKeyboard(props []*domain.UserActionProperty) *tgbotapi.InlineKeyboardMarkup {
	if values, ok := i.crm.GetLeadCustomFields(domain.InsuranceTypeLeadCustomFieldType)[domain.InsuranceTypeLeadCustomFieldType]; ok {
		rows := calculateKeyboardRows(len(values)) + 1
		sort.Strings(values)
		keyboard := make([][]tgbotapi.InlineKeyboardButton, rows)
		for i, t := range values {
			checkBox := UncheckedBox
			for _, v := range props {
				if v.Name == t && v.IsSelected == 1 {
					checkBox = CheckMark
					break
				}
			}

			keyboard[i/2] = append(keyboard[i/2], tgbotapi.NewInlineKeyboardButtonData(
				checkBox+t,
				string(domain.NewInsuranceCallbackQuery)+":"+t))
		}

		keyboard[rows-1] = tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData(i.config.BotResponses[domain.ChoosePaymentMethodKey].Text, string(domain.NewInsuranceNextStepCallbackQuery)),
		)

		return &tgbotapi.InlineKeyboardMarkup{InlineKeyboard: keyboard}
	}

	return nil
}

func (i *Implementation) NewSortedKeyboardWithCustomFieldValues(key domain.LeadCustomFieldType, callbackData domain.CallbackQuery) *tgbotapi.InlineKeyboardMarkup {
	if values, ok := i.crm.GetLeadCustomFields(key)[key]; ok {
		sort.Strings(values)
		keyboard := make([][]tgbotapi.InlineKeyboardButton, calculateKeyboardRows(len(values)))
		for i, t := range values {
			keyboard[i/2] = append(keyboard[i/2], tgbotapi.NewInlineKeyboardButtonData(t, string(callbackData)+":"+t))
		}

		return &tgbotapi.InlineKeyboardMarkup{InlineKeyboard: keyboard}
	}

	return nil
}

func (i *Implementation) ManualKeyboard() *tgbotapi.InlineKeyboardMarkup {
	values := make([]string, len(i.config.Manual.ManualTypes))
	for i, manualType := range i.config.Manual.ManualTypes {
		values[i] = manualType.Name
	}

	sort.Strings(values)
	keyboard := make([][]tgbotapi.InlineKeyboardButton, calculateKeyboardRows(len(values)))
	for i, t := range values {
		keyboard[i/2] = append(keyboard[i/2], tgbotapi.NewInlineKeyboardButtonData(t, "manual:"+t))
	}

	return &tgbotapi.InlineKeyboardMarkup{InlineKeyboard: keyboard}
}

func (i *Implementation) initKeyboards() {
	i.keyboards[MainMenuKeyboard] = &tgbotapi.ReplyKeyboardMarkup{
		Keyboard: [][]tgbotapi.KeyboardButton{
			{{Text: i.config.BotButtons[domain.InsuranceListButton]}, {Text: i.config.BotButtons[domain.InsuranceCaseButton]}},
			{{Text: i.config.BotButtons[domain.NewInsuranceButton]}, {Text: i.config.BotButtons[domain.PromocodeButton]}},
			{{Text: i.config.BotButtons[domain.ContactsButton]}, {Text: i.config.BotButtons[domain.HelpButton]}},
		},
		ResizeKeyboard: true,
	}

	i.keyboards[AuthKeyboard] = &tgbotapi.ReplyKeyboardMarkup{
		Keyboard: [][]tgbotapi.KeyboardButton{{{
			Text:           i.config.BotButtons[domain.SendPhoneNumberButton],
			RequestContact: true,
		}}, {{Text: i.config.BotButtons[domain.ContactsButton]}, {Text: i.config.BotButtons[domain.HelpButton]}}},
		ResizeKeyboard: true,
	}
}
