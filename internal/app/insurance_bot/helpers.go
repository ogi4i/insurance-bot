package insurance_bot

import (
	"context"
	"crypto/rand"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/pkg/domain"
)

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func randomCharFromCharset(charSet []byte) string {
	bytes, err := generateRandomBytes(1)
	if err != nil {
		return ""
	}
	for i, b := range bytes {
		bytes[i] = charSet[b%byte(len(charSet))]
	}
	return string(bytes)
}

func calculateKeyboardRows(length int) int {
	var rows int
	if (length % 2) == 0 {
		rows = length / 2
	} else {
		rows = (length / 2) + 1
	}
	return rows
}

func intChannelToSlice(ch chan int) []int {
	s := make([]int, len(ch))
	i := 0
	for c := range ch {
		s[i] = c
		i++
	}
	return s
}

func stringSliceContains(s []string, str string) bool {
	for _, i := range s {
		if i == str {
			return true
		}
	}

	return false
}

func intSliceContains(s []int, n int) bool {
	for _, i := range s {
		if i == n {
			return true
		}
	}

	return false
}

func isInMapValues(m map[string][]int, key string, n int) bool {
	if v, ok := m[key]; ok {
		return intSliceContains(v, n)
	}

	return false
}

func writeFileToTempDir(suffix string, key int, file *tgbotapi.FileBytes) (string, error) {
	dir := os.TempDir() + suffix + string(os.PathSeparator) + strconv.Itoa(key)
	if err := os.MkdirAll(dir, 0700); err != nil {
		if !os.IsExist(err) {
			return "", err
		}
	}

	filepath := dir + string(os.PathSeparator) + file.Name
	if err := ioutil.WriteFile(filepath, file.Bytes, 0600); err != nil {
		return "", err
	}

	log.Printf("Tempfile - Filepath: %s; Size: %d", filepath, len(file.Bytes))

	return filepath, nil
}

func (i *Implementation) GetResponseFromLead(lead *domain.Lead) string {
	response := new(strings.Builder)
	if lead.HasInsuranceTypes() {
		response.WriteString("\n" + fmt.Sprintf(
			i.config.BotResponses[domain.InsuranceTypeItemKey].Text,
			strings.Join(lead.CustomFields[domain.InsuranceTypeLeadCustomFieldType], ", ")))
	}
	if lead.HasCar() {
		response.WriteString("\n" + fmt.Sprintf(
			i.config.BotResponses[domain.CarItemKey].Text,
			strings.Join(lead.CustomFields[domain.CarLeadCustomFieldType], ", ")))
	}
	if lead.Sale != 0 {
		response.WriteString("\n" + fmt.Sprintf(
			i.config.BotResponses[domain.SaleItemKey].Text,
			lead.Sale))
	}
	if lead.HasExpiryDate() {
		response.WriteString("\n" + fmt.Sprintf(
			i.config.BotResponses[domain.ExpiryDateItemKey].Text,
			lead.PrintExpiryDate("02.01.2006")))
	}

	return response.String()
}

func (i *Implementation) GetUserIDMaps(ctx context.Context) (map[int]*domain.User, map[int][]*domain.Lead, error) {
	users, err := i.storage.GetAllUsers(ctx)
	if err != nil {
		return nil, nil, err
	}

	users = i.filterEmptyUsers(users)

	usersByUserID := make(map[int]*domain.User, len(users))
	userIds := make([]int, len(users))
	for i, user := range users {
		usersByUserID[user.Contact.ID] = user
		userIds[i] = user.Contact.ID
	}

	leadsByUserID, err := i.crm.GetLeadsByContactIDs(ctx, userIds)
	if err != nil {
		return nil, nil, err
	}

	return usersByUserID, leadsByUserID, nil
}

func (i *Implementation) GeneratePromoCode() string {
	pts := strings.Split(i.config.PromoCode.Pattern, "")
	for n, v := range pts {
		if v == "#" {
			pts[n] = randomCharFromCharset([]byte(i.config.PromoCode.Charset))
		}
	}

	return strings.Join(pts, "")
}

func (i *Implementation) FillTempFilesFromChannel(key int, filepaths chan string) {
	for filepath := range filepaths {
		if files, ok := i.tempFiles[key]; ok {
			files = append(files, filepath)
			i.tempFiles[key] = files
		} else {
			i.tempFiles[key] = []string{filepath}
		}
	}
}

func (i *Implementation) filterListInsuranceLeads(leads []*domain.Lead) []*domain.Lead {
	results := make([]*domain.Lead, 0, len(leads))
	for _, l := range leads {
		if l.IsDeleted ||
			l.IsExpired() ||
			!i.crm.IsLeadInOneOfStatuses(
				l,
				domain.ClientHasPolicyLeadstatus,
				domain.SuccessLeadStatus) ||
			!l.HasInsuranceTypes() {
			continue
		}

		results = append(results, l)
	}
	return results
}

func (i *Implementation) filterEmptyUsers(users []*domain.User) []*domain.User {
	results := make([]*domain.User, 0, len(users))
	for _, user := range users {
		if user.Contact.ID == 0 {
			continue
		}
		results = append(results, user)
	}
	return results
}
