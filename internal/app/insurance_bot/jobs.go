package insurance_bot

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"golang.org/x/sync/errgroup"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/pkg/domain"
	"insurance_bot/internal/app/pkg/telegram_bot"
)

func (i *Implementation) SendExpirationNotifications(ctx context.Context, until int) error {
	usersByUserID, leadsByUserID, err := i.GetUserIDMaps(ctx)
	if err != nil {
		return err
	}

	gUsers, ctx := errgroup.WithContext(ctx)
	for userID, leads := range leadsByUserID {
		notificationsSentChan := make(chan int, len(leads))
		gLeads, ctx := errgroup.WithContext(ctx)
		for _, lead := range leads {
			if isInMapValues(usersByUserID[userID].NotificationsSentOnLeads, strconv.Itoa(until), lead.ID) {
				continue
			}

			lead := lead
			userID := int64(usersByUserID[userID].UserID)
			gLeads.Go(func() error {
				if lead.IsDeleted ||
					!i.crm.IsLeadInOneOfStatuses(lead, domain.ClientHasPolicyLeadstatus, domain.SuccessLeadStatus) ||
					lead.IsExpired() ||
					!lead.HasInsuranceTypes() ||
					!lead.ExpiresSoon(time.Duration(until)*24*time.Hour) {
					return nil
				}
				notificationsSentChan <- lead.ID
				keyboard := tgbotapi.InlineKeyboardMarkup{
					InlineKeyboard: [][]tgbotapi.InlineKeyboardButton{
						tgbotapi.NewInlineKeyboardRow(
							tgbotapi.NewInlineKeyboardButtonData(
								i.config.BotButtons[domain.ExtendInsuranceButton],
								string(domain.ExtendInsuranceCallbackQuery)+":"+strconv.Itoa(lead.ID)),
						),
					},
				}

				return i.bot.SendMessageWithRetry(
					ctx,
					telegram_bot.NewMessage(
						userID,
						fmt.Sprintf(
							i.config.BotResponses[domain.InsuranceExpirationNotificationKey].Text,
							i.GetResponseFromLead(lead),
						),
						keyboard,
						i.config.BotResponses[domain.InsuranceExpirationNotificationKey].IsMarkdown,
					),
				)
			})
		}

		userID := userID
		gUsers.Go(func() error {
			if err := gLeads.Wait(); err != nil {
				return err
			}
			close(notificationsSentChan)

			if len(notificationsSentChan) != 0 {
				return i.storage.InsertNotifications(ctx, until, userID, intChannelToSlice(notificationsSentChan))
			}

			return nil
		})
	}

	return gUsers.Wait()
}

func (i *Implementation) sendCustomNotifications(ctx context.Context, notification *domain.Notification) error {
	g, ctx := errgroup.WithContext(ctx)
	for _, user := range notification.Users {
		userID := int64(user.UserID)
		g.Go(func() error {
			return i.bot.SendMessageWithRetry(
				ctx,
				telegram_bot.NewMessage(
					userID,
					notification.Text,
					nil,
					notification.IsMarkdown,
				),
			)
		})
	}
	return g.Wait()
}

func (i *Implementation) SendNewInsurancePDF(ctx context.Context) error {
	usersByUserID, leadsByUserID, err := i.GetUserIDMaps(ctx)
	if err != nil {
		return err
	}

	g, ctx := errgroup.WithContext(ctx)
	for userID, leads := range leadsByUserID {
		leads := leads
		userID := int64(usersByUserID[userID].UserID)
		g.Go(func() error {
			for _, lead := range leads {
				if lead.IsDeleted ||
					!i.crm.IsLeadInOneOfStatuses(lead, domain.ApprovedLeadStatus) ||
					lead.IsExpired() ||
					!lead.HasInsuranceTypes() ||
					!lead.HasDeliveryMethod("PDF") {
					continue
				}

				attachments, err := i.crm.GetPDFAttachmentsFromLead(ctx, lead.ID)
				if err != nil {
					return err
				}

				if len(attachments) != 0 {
					err := i.bot.SendMessageWithRetry(
						ctx,
						telegram_bot.NewMessage(
							userID,
							fmt.Sprintf(
								i.config.BotResponses[domain.NewInsurancePDFNotificationKey].Text,
								i.GetResponseFromLead(lead),
							),
							nil,
							i.config.BotResponses[domain.NewInsurancePDFNotificationKey].IsMarkdown))
					if err != nil {
						return err
					}

					filepaths := make(chan string, len(attachments))
					gAttachments, ctx := errgroup.WithContext(ctx)
					gFiles, ctx := errgroup.WithContext(ctx)
					for _, a := range attachments {
						file := *a
						fileP := a
						gAttachments.Go(func() error {
							return i.bot.SendMessageWithRetry(ctx, telegram_bot.NewDocument(userID, file))
						})

						gFiles.Go(func() error {
							filepath, err := writeFileToTempDir(tempDirSuffix, lead.ID, fileP)
							if err != nil {
								return err
							}
							filepaths <- filepath

							return nil
						})
					}

					if err := gAttachments.Wait(); err != nil {
						return err
					}

					if err := i.crm.ChangeLeadStatusTo(ctx, lead.ID, domain.ClientHasPolicyLeadstatus); err != nil {
						return err
					}

					if err := gFiles.Wait(); err != nil {
						return err
					}
					close(filepaths)

					i.FillTempFilesFromChannel(lead.ID, filepaths)
				}
			}

			return nil
		})
	}

	return g.Wait()
}
