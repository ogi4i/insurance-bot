package insurance_bot

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"insurance_bot/internal/app/config"
	"insurance_bot/internal/app/pkg/domain"
)

func TestCalculateKeyboardRows(t *testing.T) {
	type testCase struct {
		input    int
		expected int
	}

	tt := []testCase{
		{
			input:    1,
			expected: 1,
		},
		{
			input:    2,
			expected: 1,
		},
		{
			input:    3,
			expected: 2,
		},
		{
			input:    4,
			expected: 2,
		},
		{
			input:    5,
			expected: 3,
		},
		{
			input:    7,
			expected: 4,
		},
	}

	for _, test := range tt {
		assert.Equal(t, test.expected, calculateKeyboardRows(test.input))
	}
}

func TestGetResponseFromLead(t *testing.T) {
	type testCase struct {
		input    *domain.Lead
		expected string
	}

	var (
		testResponses = map[domain.BotResponseKey]*domain.BotResponse{
			"insurance_type_item": {
				Text: "_Тип_: *%s*",
			},
			"car_item": {
				Text: "_Автомобиль_: *%s*",
			},
			"sale_item": {
				Text: "_Общая стоимость_: *%d ₽*",
			},
			"expiry_date_item": {
				Text: "_Дата продления_: *%s*",
			},
		}

		impl = &Implementation{
			config: &config.Application{
				BotResponses: testResponses,
			},
		}

		leadWithCarSale = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.CarLeadCustomFieldType: {"Solaris"},
			},
			Sale: 111,
		}

		leadWithCar = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.CarLeadCustomFieldType: {"Solaris"},
			},
			Sale: 0,
		}

		leadWithCarsSale = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.CarLeadCustomFieldType: {"Solaris", "Rio"},
			},
			Sale: 111,
		}

		leadWithInsuranceCarsSale = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.InsuranceTypeLeadCustomFieldType: {"Осаго", "Каско"},
				domain.CarLeadCustomFieldType:           {"Solaris", "Rio"},
			},
			Sale: 111,
		}

		leadWithInsuranceCarsSaleExpire = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.InsuranceTypeLeadCustomFieldType: {"Осаго", "Каско"},
				domain.CarLeadCustomFieldType:           {"Solaris", "Rio"},
				domain.ExpiryDateLeadCustomFieldType:    {"2006-01-02 15:04:05"},
			},
			Sale: 111,
		}

		leadWithInsuranceExpire = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.InsuranceTypeLeadCustomFieldType: {"ВЗР"},
				domain.ExpiryDateLeadCustomFieldType:    {"2006-01-02 15:04:05"},
			},
		}

		leadWithInsurance = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.InsuranceTypeLeadCustomFieldType: {"ВЗР"},
			},
		}

		leadWithInsuranceSale = &domain.Lead{
			CustomFields: map[domain.LeadCustomFieldType][]string{
				domain.InsuranceTypeLeadCustomFieldType: {"ВЗР"},
			},
			Sale: 222,
		}
	)

	t.Run("single_car_without_sale_insurance_expire", func(t *testing.T) {
		tt := testCase{
			input:    leadWithCar,
			expected: "\n_Автомобиль_: *Solaris*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})

	t.Run("single_car_sale_without_insurance_expire", func(t *testing.T) {
		tt := testCase{
			input:    leadWithCarSale,
			expected: "\n_Автомобиль_: *Solaris*\n_Общая стоимость_: *111 ₽*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})

	t.Run("multiple_cars_sale_without_insurance_expire", func(t *testing.T) {
		tt := testCase{
			input:    leadWithCarsSale,
			expected: "\n_Автомобиль_: *Solaris, Rio*\n_Общая стоимость_: *111 ₽*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})

	t.Run("multiple_cars_sale_insurance_without_expire", func(t *testing.T) {
		tt := testCase{
			input:    leadWithInsuranceCarsSale,
			expected: "\n_Тип_: *Осаго, Каско*\n_Автомобиль_: *Solaris, Rio*\n_Общая стоимость_: *111 ₽*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})

	t.Run("multiple_cars_sale_insurance_expire", func(t *testing.T) {
		tt := testCase{
			input:    leadWithInsuranceCarsSaleExpire,
			expected: "\n_Тип_: *Осаго, Каско*\n_Автомобиль_: *Solaris, Rio*\n_Общая стоимость_: *111 ₽*\n_Дата продления_: *02.01.2006*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})

	t.Run("single_insurance_expire_without_car_sale", func(t *testing.T) {
		tt := testCase{
			input:    leadWithInsuranceExpire,
			expected: "\n_Тип_: *ВЗР*\n_Дата продления_: *02.01.2006*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})

	t.Run("single_insurance_without_car_sale_expire", func(t *testing.T) {
		tt := testCase{
			input:    leadWithInsurance,
			expected: "\n_Тип_: *ВЗР*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})

	t.Run("single_insurance_sale_without_car_expire", func(t *testing.T) {
		tt := testCase{
			input:    leadWithInsuranceSale,
			expected: "\n_Тип_: *ВЗР*\n_Общая стоимость_: *222 ₽*",
		}
		assert.Equal(t, tt.expected, impl.GetResponseFromLead(tt.input))
	})
}

func TestIntChannelToSlice(t *testing.T) {

	t.Run("single_value_in_buffered_channel", func(t *testing.T) {
		ch := make(chan int, 1)
		ch <- 1
		close(ch)

		assert.Equal(t, []int{1}, intChannelToSlice(ch))
	})

	t.Run("multiple_values_in_buffered_channel", func(t *testing.T) {
		ch := make(chan int, 5)
		ch <- 1
		ch <- 2
		ch <- 3
		ch <- 4
		ch <- 5
		close(ch)

		assert.Equal(t, []int{1, 2, 3, 4, 5}, intChannelToSlice(ch))
	})

	t.Run("zero_values_in_buffered_channel", func(t *testing.T) {
		ch := make(chan int, 1)
		close(ch)

		assert.Equal(t, []int{}, intChannelToSlice(ch))
	})
}

func TestIsInMapValues(t *testing.T) {
	m := map[string][]int{
		"1": {2, 3, 4},
		"5": {6, 7, 8},
		"9": {10, 11, 12},
	}

	t.Run("get_with_existant_value", func(t *testing.T) {
		assert.Equal(t, true, isInMapValues(m, "1", 3))
	})

	t.Run("get_with_non_existant_value", func(t *testing.T) {
		assert.Equal(t, false, isInMapValues(m, "1", 8))
	})

	t.Run("get_with_non_existant_key", func(t *testing.T) {
		assert.Equal(t, false, isInMapValues(m, "2", 3))
	})

	t.Run("get_with_empty_key", func(t *testing.T) {
		assert.Equal(t, false, isInMapValues(m, "", 3))
	})
}
