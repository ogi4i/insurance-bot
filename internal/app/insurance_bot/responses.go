package insurance_bot

import (
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"insurance_bot/internal/app/pkg/domain"
	"insurance_bot/internal/app/pkg/telegram_bot"
)

func (i *Implementation) AuthResponse(chatID int64) *tgbotapi.MessageConfig {
	return telegram_bot.NewMessage(
		chatID,
		i.config.BotResponses[domain.AuthorizeRequestKey].Text,
		i.keyboards[AuthKeyboard],
		i.config.BotResponses[domain.AuthorizeRequestKey].IsMarkdown)
}

func (i *Implementation) AuthSuccessResponse(name string, chatID int64) *tgbotapi.MessageConfig {
	return telegram_bot.NewMessage(
		chatID,
		fmt.Sprintf(i.config.BotResponses[domain.GreetingKey].Text, name),
		i.keyboards[MainMenuKeyboard],
		i.config.BotResponses[domain.GreetingKey].IsMarkdown)
}
