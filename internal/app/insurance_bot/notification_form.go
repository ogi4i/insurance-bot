package insurance_bot

import (
	"context"
	"io"
	"sort"

	"github.com/patrickmn/go-cache"
	uuid "github.com/satori/go.uuid"

	"insurance_bot/internal/app/pkg/domain"
)

func (i *Implementation) GetForm(w io.Writer) error {
	values := i.crm.GetLeadCustomFields(domain.InsuranceTypeLeadCustomFieldType)[domain.InsuranceTypeLeadCustomFieldType]
	sort.Strings(values)
	return i.templates.ExecuteTemplate(w, string(domain.NotificationFormTemplateType), values)
}

func (i *Implementation) NewNotification(ctx context.Context, w io.Writer, text string, insuranceTypes []string, format string) error {
	usersByUserID, leadsByUserID, err := i.GetUserIDMaps(ctx)
	if err != nil {
		return err
	}

	notification := &domain.Notification{
		Text:           text,
		InsuranceTypes: insuranceTypes,
		IsMarkdown:     format == string(domain.MarkdownNotificationFormat),
		IsAll:          stringSliceContains(insuranceTypes, "all"),
	}

	i.calculateNotificationAudience(notification, usersByUserID, leadsByUserID)

	if len(notification.Users) == 0 {
		return i.templates.ExecuteTemplate(w, string(domain.NotificationFormEmptyTemplateType), nil)
	}

	notification.UUID = uuid.NewV4().String()
	if err := i.cache.Add(notification.UUID, notification, cache.DefaultExpiration); err != nil {
		return err
	}

	return i.templates.ExecuteTemplate(w, string(domain.NotificationFormConfirmTemplateType), notification)
}

func (i *Implementation) ConfirmNotification(w io.Writer, id string) error {
	if data, ok := i.cache.Get(id); ok {
		if notification, ok := data.(*domain.Notification); ok {
			go func(notification *domain.Notification) {
				_ = i.sendCustomNotifications(context.Background(), notification)
			}(notification)

			if err := i.templates.ExecuteTemplate(w, string(domain.NotificationFormSuccessTemplateType), nil); err != nil {
				return err
			}

			i.cache.Delete(id)
		}
	}

	return nil
}

func (i *Implementation) GetErrorForm(w io.Writer) error {
	return i.templates.ExecuteTemplate(w, string(domain.NotificationFormErrorTemplateType), nil)
}

func (i *Implementation) DoesUUIDExist(uuid string) bool {
	_, ok := i.cache.Get(uuid)
	return ok
}

func (i *Implementation) calculateNotificationAudience(notification *domain.Notification, usersByUserID map[int]*domain.User, leadsByUserID map[int][]*domain.Lead) {
	usersMap := make(map[int]struct{}, len(usersByUserID))
	for userID, leads := range leadsByUserID {
		for _, lead := range leads {
			if lead.IsDeleted ||
				!i.crm.IsLeadInOneOfStatuses(lead, domain.ClientHasPolicyLeadstatus, domain.SuccessLeadStatus) ||
				lead.IsExpired() ||
				!lead.HasInsuranceTypes() ||
				(!notification.IsAll &&
					!lead.HasInsuranceTypes(notification.InsuranceTypes...)) {
				continue
			}
			usersMap[userID] = struct{}{}
		}
	}

	users := make([]*domain.User, 0, len(usersMap))
	for k := range usersMap {
		users = append(users, usersByUserID[k])
	}

	notification.Users = users
}
