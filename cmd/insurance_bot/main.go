package main

import (
	"context"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/jasonlvhit/gocron"
	"github.com/patrickmn/go-cache"

	"insurance_bot/internal/app/config"
	"insurance_bot/internal/app/insurance_bot"
	"insurance_bot/internal/app/pkg/amocrm"
	"insurance_bot/internal/app/pkg/domain"
	"insurance_bot/internal/app/pkg/http/bot_handler"
	"insurance_bot/internal/app/pkg/http/notification_form_handler"
	"insurance_bot/internal/app/pkg/repository/mongo"
	"insurance_bot/internal/app/pkg/telegram_bot"
)

func main() {
	config, err := config.ParseConfig()
	if err != nil {
		log.Fatalf("failed to parse config with error: %q", err)
	}

	templateFiles, err := filepath.Glob("templates/*")
	if err != nil {
		log.Fatalf("failed to get template filenames with glob with error: %q\n", err)
	}

	templates, err := template.ParseFiles(templateFiles...)
	if err != nil {
		log.Fatalf("failed to parse template files with error: %q\n", err)
	}

	telegramBot, err := telegram_bot.NewTelegramBot(config.Telegram)
	if err != nil {
		log.Fatalf("failed to create telegram bot with error: %q", err)
	}

	ctx := context.Background()
	amoCRMClient, err := amocrm.NewAmoCRMClient(ctx, config.AmoCRM)
	if err != nil {
		log.Fatalf("failed to create amoCRM client with error: %q", err)
	}

	mongoStore, err := mongo.NewMongoStorage(ctx, config.Database)
	if err != nil {
		log.Fatalf(err.Error())
	}

	cache := cache.New(15*time.Minute, 20*time.Minute)

	service := insurance_bot.NewInsuranceBotService(
		mongoStore,
		amoCRMClient,
		telegramBot,
		cache,
		templates,
		config.Application,
	)
	service.PrepareUserActionProperties(
		domain.InsuranceTypeLeadCustomFieldType,
		domain.PaymentMethodLeadCustomFieldType,
		domain.DeliveryMethodLeadCustomFieldType)

	cron := gocron.NewScheduler()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sigs
		cron.Clear()
		service.ClearTempDir()
		os.Exit(0)
	}()

	go func() {
		gocron.ChangeLoc(config.Application.TimeZone.ToLocation())
		for _, t := range config.Application.Notifications.ExpirationNotifications {
			cron.Every(1).Day().At(config.Application.ExtendInsurance.NotifyAt).Do(service.SendExpirationNotifications, ctx, t) //nolint: errcheck
		}
		cron.Every(config.Application.Notifications.PDFNotification).Minutes().Do(service.SendNewInsurancePDF, ctx) //nolint: errcheck
		<-cron.Start()
	}()

	botHandler := bot_handler.NewHandler(service)

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Route("/"+config.Telegram.Token, func(r chi.Router) {
		r.Use(middleware.AllowContentType("application/json"))
		r.Use(render.SetContentType(render.ContentTypeJSON))
		r.With(bot_handler.LogTelegramWebhooks).Post("/", botHandler.HandleUpdate)
	})

	if config.Application.NotificationForm.Enabled {
		notificationFormHandler := notification_form_handler.NewHandler(
			service,
			config.Application.NotificationForm.Authentication,
		)

		r.Route("/", func(r chi.Router) {
			r.Use(middleware.AllowContentType("application/x-www-form-urlencoded"))
			r.Use(notificationFormHandler.BasicAuth)
			r.Get("/", notificationFormHandler.GetForm)
			r.Post("/", notificationFormHandler.NewNotification)
			r.With(notificationFormHandler.EnsureUUIDExists).Post("/confirm/{uuid:[a-f0-9-]{36}}", notificationFormHandler.ConfirmNotification)
			r.Get("/error", notificationFormHandler.GetErrorForm)
		})
	}

	_ = http.ListenAndServe(":3335", r)
}
