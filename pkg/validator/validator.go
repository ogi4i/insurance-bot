package validator

import (
	"regexp"

	"gopkg.in/go-playground/validator.v9"
)

func mongoDSNValidator(fl validator.FieldLevel) bool {
	if ok, _ := regexp.MatchString(`^mongodb://([a-zA-Z0-9][a-zA-Z0-9.-]+[a-zA-Z0-9]|(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])):([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$`, fl.Field().String()); !ok {
		return false
	}

	return true
}

func RegisterValidators(v *validator.Validate) error {
	if err := v.RegisterValidation("mongodsn", mongoDSNValidator); err != nil {
		return err
	}

	return nil
}
